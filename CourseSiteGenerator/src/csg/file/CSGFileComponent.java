package csg.file;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.CourseData;
import csg.data.ProjectData;
import csg.data.ProjectStudent;
import csg.data.ProjectTeam;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.data.ScheduleItemType;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.workspace.Workspace;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Luke Cesario
 */
public class CSGFileComponent implements AppFileComponent {

    CSGApplication app;
    TAFiles taFile;

    static final String JSON_COURSE = "course";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_NUMBER = "number";
    static final String JSON_YEAR = "year";
    static final String JSON_TITLE = "title";
    static final String JSON_INSTRUCTOR_NAME = "instructor";
    static final String JSON_INSTRUCTOR_HOME = "instructor_home";

    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAY_TIME = "day_and_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_SUPERVISING_TA1 = "supervising_TA1";
    static final String JSON_SUPERVISING_TA2 = "supervising_TA2";

    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_UNDER = "undergrad";

    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_HOLIDAY = "holidays";
    static final String JSON_LECTURE = "lectures";
    static final String JSON_REC_ITEM = "recitations";
    static final String JSON_REFERENCE = "references";
    static final String JSON_HW = "hws";
    static final String JSON_ITEMS = "items";
    static final String JSON_MONDAY_YEAR = "startingMondayYear";
    static final String JSON_MONDAY_MONTH = "startingMondayMonth";
    static final String JSON_MONDAY_DAY = "startingMondayDay";
    static final String JSON_FRIDAY_YEAR = "endingFridayYear";
    static final String JSON_FRIDAY_MONTH = "endingFridayMonth";
    static final String JSON_FRIDAY_DAY = "endingFridayDay";
    static final String JSON_ITEM_YEAR = "year";
    static final String JSON_ITEM_MONTH = "month";
    static final String JSON_ITEM_DAY = "day";
    static final String JSON_ITEM_TIME = "time";
    static final String JSON_ITEM_TITLE = "title";
    static final String JSON_ITEM_TOPIC = "topic";
    static final String JSON_ITEM_CRITERIA = "criteria";

    static final String JSON_TEAM_NAME = "name";
    static final String JSON_COLOR = "color";//to be change to rgb
    static final String JSON_TEXT_COLOR = "text_color";
    static final String JSON_MEMBERS = "students";
    static final String JSON_MEMBER_FIRST_NAME = "first_name";
    static final String JSON_MEMBER_LAST_NAME = "last_name";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_PROJECTS = "projects";
    static final String JSON_TEAMS = "teams";
    static final String JSON_STUDENTS = "students";

    static final String JSON_WORK = "work";

    static final String JSON_LINK = "link";

    public CSGFileComponent(CSGApplication initApp) {
        app = initApp;
        taFile = new TAFiles(app);
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        CSGDataComponent dataManager = (CSGDataComponent) data;

        CourseData courseData = dataManager.getCourseData();

        JsonObject courseBuilder = Json.createObjectBuilder()
                .add(JSON_SUBJECT, courseData.getSubject())
                .add(JSON_SEMESTER, courseData.getSemester())
                .add(JSON_NUMBER, courseData.getNumber())
                .add(JSON_YEAR, courseData.getYear())
                .add(JSON_TITLE, courseData.getTitle())
                .add(JSON_INSTRUCTOR_NAME, courseData.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, courseData.getInstructorHome())
                .build();

        TAData tadata = dataManager.getTaData();

        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = tadata.getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDER, ta.getUndergrad()).build();
            taArrayBuilder.add(taJson);
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();


        /*JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        ArrayList<TimeSlot> timeSlots = TimeSlot.buildOfficeHoursList(tadata);
        for (TimeSlot ts : timeSlots) {
            JsonObject tsJson = Json.createObjectBuilder()
                    .add(JSON_DAY, ts.getDay())
                    .add(JSON_TIME, ts.getTime())
                    .add(JSON_NAME, ts.getName()).build();
            timeSlotArrayBuilder.add(tsJson);
        }
        JsonArray timeSlotsArray = timeSlotArrayBuilder.build();*/
        JsonObject officeHours = Json.createObjectBuilder()
                .add(JSON_START_HOUR, "" + tadata.getStartHour())
                .add(JSON_END_HOUR, "" + tadata.getEndHour())
                .build();

        RecitationData recData = dataManager.getRecData();
        JsonArrayBuilder recitationsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = recData.getRecitations();
        for (Recitation r : recitations) {
            String ta1 = "";
            String ta2 = "";
            if (r.getSupervisingTA1() != null) {
                ta1 = r.getSupervisingTA1().getName();
            }
            if (r.getSupervisingTA2() != null) {
                ta2 = r.getSupervisingTA2().getName();
            }
            JsonObject recJson = Json.createObjectBuilder()
                    .add(JSON_INSTRUCTOR, r.getInstructor())
                    .add(JSON_SECTION, r.getSection())
                    .add(JSON_DAY_TIME, r.getDayAndTime())
                    .add(JSON_LOCATION, r.getLocation())
                    .add(JSON_SUPERVISING_TA1, ta1)
                    .add(JSON_SUPERVISING_TA2, ta2).build();
            recitationsArrayBuilder.add(recJson);
        }
        JsonArray recitationsArray = recitationsArrayBuilder.build();

        ScheduleData scheduleData = dataManager.getScheduleData();
        JsonArrayBuilder holidayItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> holidays = scheduleData.getHolidayItems();
        for (ScheduleItem holiday : holidays) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, holiday.getDate().getYear())
                    .add(JSON_ITEM_MONTH, holiday.getMonth())
                    .add(JSON_ITEM_DAY, holiday.getDay())
                    .add(JSON_ITEM_TIME, holiday.getTime())
                    .add(JSON_ITEM_TITLE, holiday.getTitle())
                    .add(JSON_ITEM_TOPIC, holiday.getTopic())
                    .add(JSON_LINK, holiday.getLink())
                    .add(JSON_ITEM_CRITERIA, holiday.getCriteria()).build();
            holidayItems.add(itemJson);
        }
        JsonArrayBuilder lectureItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> lectures = scheduleData.getLectureItems();
        for (ScheduleItem lecture : lectures) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, lecture.getDate().getYear())
                    .add(JSON_ITEM_MONTH, lecture.getMonth())
                    .add(JSON_ITEM_DAY, lecture.getDay())
                    .add(JSON_ITEM_TIME, lecture.getTime())
                    .add(JSON_ITEM_TITLE, lecture.getTitle())
                    .add(JSON_ITEM_TOPIC, lecture.getTopic())
                    .add(JSON_LINK, lecture.getLink()).build();
            lectureItems.add(itemJson);
        }
        JsonArrayBuilder hwItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> hws = scheduleData.getLectureItems();
        for (ScheduleItem hw : hws) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, hw.getDate().getYear())
                    .add(JSON_ITEM_MONTH, hw.getMonth())
                    .add(JSON_ITEM_DAY, hw.getDay())
                    .add(JSON_ITEM_TIME, hw.getTime())
                    .add(JSON_ITEM_TITLE, hw.getTitle())
                    .add(JSON_ITEM_TOPIC, hw.getTopic())
                    .add(JSON_LINK, hw.getLink()).build();
            hwItems.add(itemJson);
        }
        JsonArrayBuilder recItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> recitationList = scheduleData.getRecitationItems();
        for (ScheduleItem recitation : recitationList) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, recitation.getDate().getYear())
                    .add(JSON_ITEM_MONTH, recitation.getMonth())
                    .add(JSON_ITEM_DAY, recitation.getDay())
                    .add(JSON_ITEM_TIME, recitation.getTime())
                    .add(JSON_ITEM_TITLE, recitation.getTitle())
                    .add(JSON_ITEM_TOPIC, recitation.getTopic())
                    .add(JSON_LINK, recitation.getLink()).build();
            recItems.add(itemJson);
        }
        JsonArrayBuilder referenceItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> references = scheduleData.getReferenceItems();
        for (ScheduleItem reference : references) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, reference.getDate().getYear())
                    .add(JSON_ITEM_MONTH, reference.getMonth())
                    .add(JSON_ITEM_DAY, reference.getDay())
                    .add(JSON_ITEM_TIME, reference.getTime())
                    .add(JSON_ITEM_TITLE, reference.getTitle())
                    .add(JSON_ITEM_TOPIC, reference.getTopic())
                    .add(JSON_LINK, reference.getLink()).build();
            referenceItems.add(itemJson);
        }
        JsonObject scheduleJSO = Json.createObjectBuilder()
                .add(JSON_MONDAY_YEAR, scheduleData.getStartingMondayYear())
                .add(JSON_MONDAY_MONTH, scheduleData.getStartingMondayMonth())
                .add(JSON_MONDAY_DAY, scheduleData.getStartingMondayDay())
                .add(JSON_FRIDAY_YEAR, scheduleData.getEndingFridayYear())
                .add(JSON_FRIDAY_MONTH, scheduleData.getEndingFridayMonth())
                .add(JSON_FRIDAY_DAY, scheduleData.getEndingFridayDay())
                .add(JSON_HOLIDAY, holidayItems.build())
                .add(JSON_LECTURE, lectureItems.build())
                .add(JSON_HW, hwItems.build())
                .add(JSON_REC_ITEM, recItems.build())
                .add(JSON_REFERENCE, referenceItems.build()).build();

        ProjectData projectData = dataManager.getProjectData();
        JsonArrayBuilder teamsJSO = Json.createArrayBuilder();
        for (ProjectTeam t : projectData.getTeams()) {
            JsonObjectBuilder teamjson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, t.getName())
                    .add(JSON_COLOR, t.getColor())
                    .add(JSON_TEXT_COLOR, t.getTextColor())
                    .add(JSON_LINK, t.getLink());
            JsonArrayBuilder members = Json.createArrayBuilder();
            for (ProjectStudent s : t.getMembers()) {
                members.add(s.getFirstName() + " " + s.getLastName());
            }
            teamjson.add(JSON_MEMBERS, members);
            teamsJSO.add(teamjson.build());
        }

        JsonArrayBuilder studentsJSO = Json.createArrayBuilder();
        for (ProjectStudent s : projectData.getStudents()) {
            JsonObject studentjson = Json.createObjectBuilder()
                    .add(JSON_MEMBER_LAST_NAME, s.getLastName())
                    .add(JSON_MEMBER_FIRST_NAME, s.getFirstName())
                    .add(JSON_TEAM, s.getTeam())
                    .add(JSON_ROLE, s.getRole()).build();
            studentsJSO.add(studentjson);
        }

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_COURSE, courseBuilder)
                .add(JSON_OFFICE_HOURS, officeHours)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                //.add(JSON_OFFICE_HOURS, timeSlotsArray)
                .add(JSON_RECITATIONS, recitationsArray)
                .add(JSON_SCHEDULE, scheduleJSO)
                .add(JSON_TEAMS, teamsJSO.build())
                .add(JSON_STUDENTS, studentsJSO.build())
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        FileUtils.deleteQuietly(new File(filePath));
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        CSGDataComponent dataManager = (CSGDataComponent) data;
        JsonObject json = loadJSONFile(filePath);
        //Load CourseData
        String subject = json.getJsonObject(JSON_COURSE).getString(JSON_SUBJECT);
        String semester = json.getJsonObject(JSON_COURSE).getString(JSON_SEMESTER);
        int courseNum = json.getJsonObject(JSON_COURSE).getInt(JSON_NUMBER);
        int year = json.getJsonObject(JSON_COURSE).getInt(JSON_YEAR);
        String courseTitle = json.getJsonObject(JSON_COURSE).getString(JSON_TITLE);
        String instructorName = json.getJsonObject(JSON_COURSE).getString(JSON_INSTRUCTOR_NAME);
        String instructorHome = json.getJsonObject(JSON_COURSE).getString(JSON_INSTRUCTOR_HOME);
        dataManager.getCourseData().initData(subject, semester, courseNum, year, courseTitle, instructorName, instructorHome);

        String startHour = json.getJsonObject(JSON_OFFICE_HOURS).getString(JSON_START_HOUR);
        String endHour = json.getJsonObject(JSON_OFFICE_HOURS).getString(JSON_END_HOUR);
        dataManager.getTaData().initHours(startHour, endHour);

        for (int i = 0; i < json.getJsonArray(JSON_UNDERGRAD_TAS).size(); i++) {
            String name = json.getJsonArray(JSON_UNDERGRAD_TAS).getJsonObject(i).getString(JSON_NAME);
            String email = json.getJsonArray(JSON_UNDERGRAD_TAS).getJsonObject(i).getString(JSON_EMAIL);
            boolean undergrad = json.getJsonArray(JSON_UNDERGRAD_TAS).getJsonObject(i).getBoolean(JSON_UNDER);
            dataManager.getTaData().addTA(name, email, undergrad);
        }

        for (int i = 0; i < json.getJsonArray(JSON_RECITATIONS).size(); i++) {
            String instructor = json.getJsonArray(JSON_RECITATIONS).getJsonObject(i).getString(JSON_INSTRUCTOR);
            String section = json.getJsonArray(JSON_RECITATIONS).getJsonObject(i).getString(JSON_SECTION);
            String dayTime = json.getJsonArray(JSON_RECITATIONS).getJsonObject(i).getString(JSON_DAY_TIME);
            String location = json.getJsonArray(JSON_RECITATIONS).getJsonObject(i).getString(JSON_LOCATION);
            String ta1 = json.getJsonArray(JSON_RECITATIONS).getJsonObject(i).getString(JSON_SUPERVISING_TA1);
            String ta2 = json.getJsonArray(JSON_RECITATIONS).getJsonObject(i).getString(JSON_SUPERVISING_TA2);
            dataManager.getRecData().getRecitations().add(new Recitation(instructor, section, dayTime,
                    location, dataManager.getTaData().getTA(ta1), dataManager.getTaData().getTA(ta2)));
        }

        int monYear = json.getInt(JSON_MONDAY_YEAR);
        int monMonth = json.getInt(JSON_MONDAY_MONTH);
        int monDay = json.getInt(JSON_MONDAY_DAY);
        int friYear = json.getInt(JSON_FRIDAY_YEAR);
        int friMonth = json.getInt(JSON_FRIDAY_MONTH);
        int friDay = json.getInt(JSON_FRIDAY_DAY);
        for (int i = 0; i < json.getJsonArray(JSON_HOLIDAY).size(); i++) {
            int month = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getInt(JSON_ITEM_MONTH);
            int day = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getInt(JSON_ITEM_DAY);
            int itemYear = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getInt(JSON_ITEM_YEAR);
            String time = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getString(JSON_ITEM_TIME);
            String title = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getString(JSON_ITEM_TITLE);
            String topic = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getString(JSON_ITEM_TOPIC);
            String link = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getString(JSON_LINK);
            String criteria = json.getJsonArray(JSON_HOLIDAY).getJsonObject(i).getString(JSON_ITEM_CRITERIA);
            dataManager.getScheduleData().getHolidayItems().add(new ScheduleItem(ScheduleItemType.HOLIDAY, LocalDate.of(itemYear, month, day), time, title, topic, link, criteria));
        }
        for (int i = 0; i < json.getJsonArray(JSON_LECTURE).size(); i++) {
            int month = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getInt(JSON_ITEM_MONTH);
            int day = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getInt(JSON_ITEM_DAY);
            int itemYear = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getInt(JSON_ITEM_YEAR);
            String time = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getString(JSON_ITEM_TIME);
            String title = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getString(JSON_ITEM_TITLE);
            String topic = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getString(JSON_ITEM_TOPIC);
            String link = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getString(JSON_LINK);
            String criteria = json.getJsonArray(JSON_LECTURE).getJsonObject(i).getString(JSON_ITEM_CRITERIA);
            dataManager.getScheduleData().getLectureItems().add(new ScheduleItem(ScheduleItemType.LECTURE, LocalDate.of(itemYear, month, day), time, title, topic, link, criteria));
        }
        for (int i = 0; i < json.getJsonArray(JSON_HW).size(); i++) {
            int month = json.getJsonArray(JSON_HW).getJsonObject(i).getInt(JSON_ITEM_MONTH);
            int day = json.getJsonArray(JSON_HW).getJsonObject(i).getInt(JSON_ITEM_DAY);
            int itemYear = json.getJsonArray(JSON_HW).getJsonObject(i).getInt(JSON_ITEM_YEAR);
            String time = json.getJsonArray(JSON_HW).getJsonObject(i).getString(JSON_ITEM_TIME);
            String title = json.getJsonArray(JSON_HW).getJsonObject(i).getString(JSON_ITEM_TITLE);
            String topic = json.getJsonArray(JSON_HW).getJsonObject(i).getString(JSON_ITEM_TOPIC);
            String link = json.getJsonArray(JSON_HW).getJsonObject(i).getString(JSON_LINK);
            String criteria = json.getJsonArray(JSON_HW).getJsonObject(i).getString(JSON_ITEM_CRITERIA);
            dataManager.getScheduleData().getHwItems().add(new ScheduleItem(ScheduleItemType.HW, LocalDate.of(itemYear, month, day), time, title, topic, link, criteria));
        }
        for (int i = 0; i < json.getJsonArray(JSON_REC_ITEM).size(); i++) {
            int month = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getInt(JSON_ITEM_MONTH);
            int day = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getInt(JSON_ITEM_DAY);
            int itemYear = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getInt(JSON_ITEM_YEAR);
            String time = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getString(JSON_ITEM_TIME);
            String title = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getString(JSON_ITEM_TITLE);
            String topic = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getString(JSON_ITEM_TOPIC);
            String link = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getString(JSON_LINK);
            String criteria = json.getJsonArray(JSON_REC_ITEM).getJsonObject(i).getString(JSON_ITEM_CRITERIA);
            dataManager.getScheduleData().getRecitationItems().add(new ScheduleItem(ScheduleItemType.RECITATION, LocalDate.of(itemYear, month, day), time, title, topic, link, criteria));
        }
        for (int i = 0; i < json.getJsonArray(JSON_REFERENCE).size(); i++) {
            int month = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getInt(JSON_ITEM_MONTH);
            int day = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getInt(JSON_ITEM_DAY);
            int itemYear = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getInt(JSON_ITEM_YEAR);
            String time = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getString(JSON_ITEM_TIME);
            String title = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getString(JSON_ITEM_TITLE);
            String topic = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getString(JSON_ITEM_TOPIC);
            String link = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getString(JSON_LINK);
            String criteria = json.getJsonArray(JSON_REFERENCE).getJsonObject(i).getString(JSON_ITEM_CRITERIA);
            dataManager.getScheduleData().getReferenceItems().add(new ScheduleItem(ScheduleItemType.REFERENCE, LocalDate.of(itemYear, month, day), time, title, topic, link, criteria));
        }
        for (int i = 0; i < json.getJsonArray(JSON_STUDENTS).size(); i++) {
            String lastName = json.getJsonArray(JSON_STUDENTS).getJsonObject(i).getString(JSON_MEMBER_LAST_NAME);
            String firstName = json.getJsonArray(JSON_STUDENTS).getJsonObject(i).getString(JSON_MEMBER_FIRST_NAME);
            String team = json.getJsonArray(JSON_STUDENTS).getJsonObject(i).getString(JSON_TEAM);
            String role = json.getJsonArray(JSON_STUDENTS).getJsonObject(i).getString(JSON_ROLE);
            dataManager.getProjectData().getStudents().add(new ProjectStudent(firstName, lastName, team, role));
        }

        for (int i = 0; i < json.getJsonArray(JSON_TEAMS).size(); i++) {
            String name = json.getJsonArray(JSON_TEAMS).getJsonObject(i).getString(JSON_TEAM_NAME);
            String color = json.getJsonArray(JSON_TEAMS).getJsonObject(i).getString(JSON_COLOR);
            String textColor = json.getJsonArray(JSON_TEAMS).getJsonObject(i).getString(JSON_TEXT_COLOR);
            String link = json.getJsonArray(JSON_TEAMS).getJsonObject(i).getString(JSON_LINK);
            ProjectTeam team = new ProjectTeam(name, color, textColor, link);
            dataManager.getProjectData().getTeams().add(team);
            for (int j = 0; j < json.getJsonArray(JSON_TEAMS).getJsonObject(i).getJsonArray(JSON_STUDENTS).size(); j++) {
                String student = json.getJsonArray(JSON_TEAMS).getJsonObject(i).getJsonArray(JSON_STUDENTS).getString(j);
                ProjectStudent s = dataManager.getProjectData().getStudent(student);
                if (s != null) {
                    team.addMember(s);
                }
            }
        }
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
        
        
        CSGDataComponent dataManager = (CSGDataComponent) data;

        CourseData courseData = dataManager.getCourseData();
        File export = new File(courseData.getExportDir());
        FileUtils.copyDirectory(new File(dataManager.getTemplateDir()), export);

        JsonObject courseBuilder = Json.createObjectBuilder()
                .add(JSON_SUBJECT, courseData.getSubject())
                .add(JSON_SEMESTER, courseData.getSemester())
                .add(JSON_NUMBER, courseData.getNumber())
                .add(JSON_YEAR, courseData.getYear())
                .add(JSON_TITLE, courseData.getTitle())
                .add(JSON_INSTRUCTOR_NAME, courseData.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, courseData.getInstructorHome())
                .build();

        TAData tadata = dataManager.getTaData();

        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = tadata.getTeachingAssistants();
        for (TeachingAssistant ta : tas) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_UNDER, ta.getUndergrad()).build();
            taArrayBuilder.add(taJson);
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();


        /*JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        ArrayList<TimeSlot> timeSlots = TimeSlot.buildOfficeHoursList(tadata);
        for (TimeSlot ts : timeSlots) {
            JsonObject tsJson = Json.createObjectBuilder()
                    .add(JSON_DAY, ts.getDay())
                    .add(JSON_TIME, ts.getTime())
                    .add(JSON_NAME, ts.getName()).build();
            timeSlotArrayBuilder.add(tsJson);
        }
        JsonArray timeSlotsArray = timeSlotArrayBuilder.build();*/
        JsonObject officeHours = Json.createObjectBuilder()
                .add(JSON_START_HOUR, "" + tadata.getStartHour())
                .add(JSON_END_HOUR, "" + tadata.getEndHour())
                .build();

        RecitationData recData = dataManager.getRecData();
        JsonArrayBuilder recitationsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = recData.getRecitations();
        
        for (Recitation r : recitations) {
            String ta1 = "";
            String ta2 = "";
            if (r.getSupervisingTA1() != null) {
                ta1 = r.getSupervisingTA1().getName();
            }
            if (r.getSupervisingTA2() != null) {
                ta2 = r.getSupervisingTA2().getName();
            }
            JsonObject recJson = Json.createObjectBuilder()
                    .add(JSON_INSTRUCTOR, r.getInstructor())
                    .add(JSON_SECTION, r.getSection())
                    .add(JSON_DAY_TIME, r.getDayAndTime())
                    .add(JSON_LOCATION, r.getLocation())
                    .add(JSON_SUPERVISING_TA1, ta1)
                    .add(JSON_SUPERVISING_TA2, ta2).build();
            recitationsArrayBuilder.add(recJson);
        }
        JsonArray recitationsArray = recitationsArrayBuilder.build();

        ScheduleData scheduleData = dataManager.getScheduleData();
        JsonArrayBuilder holidayItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> holidays = scheduleData.getHolidayItems();
        for (ScheduleItem holiday : holidays) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, holiday.getDate().getYear())
                    .add(JSON_ITEM_MONTH, holiday.getMonth())
                    .add(JSON_ITEM_DAY, holiday.getDay())
                    .add(JSON_ITEM_TIME, holiday.getTime())
                    .add(JSON_ITEM_TITLE, holiday.getTitle())
                    .add(JSON_ITEM_TOPIC, holiday.getTopic())
                    .add(JSON_LINK, holiday.getLink())
                    .add(JSON_ITEM_CRITERIA, holiday.getCriteria()).build();
            holidayItems.add(itemJson);
        }
        JsonArrayBuilder lectureItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> lectures = scheduleData.getLectureItems();
        for (ScheduleItem lecture : lectures) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, lecture.getDate().getYear())
                    .add(JSON_ITEM_MONTH, lecture.getMonth())
                    .add(JSON_ITEM_DAY, lecture.getDay())
                    .add(JSON_ITEM_TIME, lecture.getTime())
                    .add(JSON_ITEM_TITLE, lecture.getTitle())
                    .add(JSON_ITEM_TOPIC, lecture.getTopic())
                    .add(JSON_LINK, lecture.getLink()).build();
            lectureItems.add(itemJson);
        }
        JsonArrayBuilder hwItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> hws = scheduleData.getLectureItems();
        for (ScheduleItem hw : hws) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, hw.getDate().getYear())
                    .add(JSON_ITEM_MONTH, hw.getMonth())
                    .add(JSON_ITEM_DAY, hw.getDay())
                    .add(JSON_ITEM_TIME, hw.getTime())
                    .add(JSON_ITEM_TITLE, hw.getTitle())
                    .add(JSON_ITEM_TOPIC, hw.getTopic())
                    .add(JSON_LINK, hw.getLink()).build();
            hwItems.add(itemJson);
        }
        JsonArrayBuilder recItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> recitationList = scheduleData.getRecitationItems();
        for (ScheduleItem recitation : recitationList) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, recitation.getDate().getYear())
                    .add(JSON_ITEM_MONTH, recitation.getMonth())
                    .add(JSON_ITEM_DAY, recitation.getDay())
                    .add(JSON_ITEM_TIME, recitation.getTime())
                    .add(JSON_ITEM_TITLE, recitation.getTitle())
                    .add(JSON_ITEM_TOPIC, recitation.getTopic())
                    .add(JSON_LINK, recitation.getLink()).build();
            recItems.add(itemJson);
        }
        JsonArrayBuilder referenceItems = Json.createArrayBuilder();
        ObservableList<ScheduleItem> references = scheduleData.getReferenceItems();
        for (ScheduleItem reference : references) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ITEM_YEAR, reference.getDate().getYear())
                    .add(JSON_ITEM_MONTH, reference.getMonth())
                    .add(JSON_ITEM_DAY, reference.getDay())
                    .add(JSON_ITEM_TIME, reference.getTime())
                    .add(JSON_ITEM_TITLE, reference.getTitle())
                    .add(JSON_ITEM_TOPIC, reference.getTopic())
                    .add(JSON_LINK, reference.getLink()).build();
            referenceItems.add(itemJson);
        }
        JsonObject scheduleJSO = Json.createObjectBuilder()
                .add(JSON_MONDAY_YEAR, scheduleData.getStartingMondayYear())
                .add(JSON_MONDAY_MONTH, scheduleData.getStartingMondayMonth())
                .add(JSON_MONDAY_DAY, scheduleData.getStartingMondayDay())
                .add(JSON_FRIDAY_YEAR, scheduleData.getEndingFridayYear())
                .add(JSON_FRIDAY_MONTH, scheduleData.getEndingFridayMonth())
                .add(JSON_FRIDAY_DAY, scheduleData.getEndingFridayDay())
                .add(JSON_HOLIDAY, holidayItems.build())
                .add(JSON_LECTURE, lectureItems.build())
                .add(JSON_HW, hwItems.build())
                .add(JSON_REC_ITEM, recItems.build())
                .add(JSON_REFERENCE, referenceItems.build()).build();
   
        ProjectData projectData = dataManager.getProjectData();
        JsonArrayBuilder teamsJSO = Json.createArrayBuilder();
        for (ProjectTeam t : projectData.getTeams()) {
            JsonObjectBuilder teamjson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, t.getName())
                    .add(JSON_COLOR, t.getColor())
                    .add(JSON_TEXT_COLOR, t.getTextColor())
                    .add(JSON_LINK, t.getLink());
            JsonArrayBuilder members = Json.createArrayBuilder();
            for (ProjectStudent s : t.getMembers()) {
                members.add(s.getFirstName() + " " + s.getLastName());
            }
            teamjson.add(JSON_MEMBERS, members);
            teamsJSO.add(teamjson.build());
        }

        JsonArrayBuilder studentsJSO = Json.createArrayBuilder();
        for (ProjectStudent s : projectData.getStudents()) {
            JsonObject studentjson = Json.createObjectBuilder()
                    .add(JSON_MEMBER_LAST_NAME, s.getLastName())
                    .add(JSON_MEMBER_FIRST_NAME, s.getFirstName())
                    .add(JSON_TEAM, s.getTeam())
                    .add(JSON_ROLE, s.getRole()).build();
            studentsJSO.add(studentjson);
        }
        JsonObjectBuilder projectsJSO = Json.createObjectBuilder();
        JsonArrayBuilder projectsJSA = Json.createArrayBuilder();
        for (ProjectTeam t : projectData.getTeams()) {
            JsonObjectBuilder projectjson = Json.createObjectBuilder().add(JSON_TEAM_NAME, t.getName());

            JsonArrayBuilder members = Json.createArrayBuilder();
            for (ProjectStudent s : t.getMembers()) {
                members.add(s.getFirstName() + " " + s.getLastName());
            }
            projectjson.add(JSON_MEMBERS, members);
            projectjson.add(JSON_LINK, t.getLink());
            projectsJSA.add(projectjson.build());
        }
        projectsJSO.add(JSON_SEMESTER, courseData.getSemester() + " " +courseData.getYear());
        projectsJSO.add(JSON_PROJECTS, projectsJSA);

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        /*JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_COURSE, courseBuilder)
                .add(JSON_OFFICE_HOURS, officeHours)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                */
        JsonObject recitationsJSO = Json.createObjectBuilder().add(JSON_RECITATIONS, recitationsArray).build();
        File recjson = makeNewJSON(recitationsJSO, export.getAbsolutePath() + "\\syllabus_files\\RecitationsData.json", courseData);
        File scjson = makeNewJSON(scheduleJSO, export.getAbsolutePath() + "\\schedule_files\\ScheduleData.json", courseData);
        File sc2json = makeNewJSON(scheduleJSO, export.getAbsolutePath() + "\\hws_files\\ScheduleData.json", courseData);
        JsonObject teamsStudentsJSO = Json.createObjectBuilder().add(JSON_TEAMS, teamsJSO.build()).add(JSON_STUDENTS, studentsJSO.build()).build();
        File teamstjson = makeNewJSON(teamsStudentsJSO, export.getAbsolutePath() + "\\syllabus_files\\TeamsAndStudents.json", courseData);
        JsonObject projects = Json.createObjectBuilder().add(JSON_WORK, projectsJSO.build()).build();
        File projjson = makeNewJSON(projects, export.getAbsolutePath() + "\\projects_files\\ProjectsData", courseData);
        
        

    }

    private File makeNewJSON(JsonObject jso, String filePath, CourseData data) throws FileNotFoundException {
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(jso);
        jsonWriter.close();
        File json = new File(filePath);
        System.out.println(json.getAbsolutePath());
        OutputStream os = new FileOutputStream(json.getAbsolutePath());
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(jso);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(json.getAbsolutePath());
        pw.write(prettyPrinted);
        pw.close();
        properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        writerFactory = Json.createWriterFactory(properties);
        sw = new StringWriter();
        jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(jso);
        jsonWriter.close();
        return json;
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    static JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

}
