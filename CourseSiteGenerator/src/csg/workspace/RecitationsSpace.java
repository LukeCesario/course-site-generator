package csg.workspace;

import csg.CSGApplication;
import csg.Properties.RecitationsProp;
import csg.data.CSGDataComponent;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.uistate.RecitationsSpaceController;
import djf.components.AppDataComponent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author onear
 */
public class RecitationsSpace {

    private Tab recitationsTab;
    private VBox recitationsPane;

    private HBox recitationsHeaderBox;
    private TableView<Recitation> recTable;
    private TableColumn<Recitation, String> sectionCol;
    private TableColumn<Recitation, String> instructorCol;
    private TableColumn<Recitation, String> dayTimeCol;
    private TableColumn<Recitation, String> locationCol;
    private TableColumn<Recitation, String> TA1Col;
    private TableColumn<Recitation, String> TA2Col;

    private Label recitationsHeaderLabel;
    private Label addEditHeaderLabel;
    private Label deleteButtonLabel;
    private Label addUpdateButtonLabel;
    private Label clearButtonLabel;
    private Label sectionLabel;
    private Label instructorLabel;
    private Label locationLabel;
    private Label dayTimeLabel;
    private Label supervisingLabel;
    private Label supervisingLabel2;
    private Label emptyTableLabel;

    private Button deleteButton;
    private Button clearButton;
    private Button addUpdateButton;

    private VBox addEditPane;
    private GridPane editingToolsPane;

    private TextField sectionField;
    private TextField instructorField;
    private TextField dayTimeField;
    private TextField locationField;
    private ComboBox supervisingCombo;
    private ComboBox supervisingCombo2;

    private CSGApplication app;
    private RecitationsSpaceController controller;

    private boolean updateMode;

    public RecitationsSpace(CSGApplication app) {
        this.app = app;

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        recTable = new TableView<>();
        sectionCol = new TableColumn<>(props.getProperty(RecitationsProp.SECTION_COL_TEXT.toString()));
        sectionCol.setCellValueFactory(new PropertyValueFactory<>("section"));
        sectionCol.prefWidthProperty().bind(recTable.widthProperty().multiply(0.17));
        instructorCol = new TableColumn<>(props.getProperty(RecitationsProp.INSTRUCTOR_COL_TEXT.toString()));
        instructorCol.setCellValueFactory(new PropertyValueFactory<>("instructor"));
        instructorCol.prefWidthProperty().bind(recTable.widthProperty().multiply(0.17));
        dayTimeCol = new TableColumn<>(props.getProperty(RecitationsProp.DAYTIME_COL_TEXT.toString()));
        dayTimeCol.setCellValueFactory(new PropertyValueFactory<>("dayAndTime"));
        dayTimeCol.prefWidthProperty().bind(recTable.widthProperty().multiply(0.17));
        locationCol = new TableColumn<>(props.getProperty(RecitationsProp.LOCATION_COL_TEXT.toString()));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        locationCol.prefWidthProperty().bind(recTable.widthProperty().multiply(0.17));
        TA1Col = new TableColumn<>(props.getProperty(RecitationsProp.TA_COL_TEXT.toString()));
        TA1Col.setCellValueFactory(new PropertyValueFactory<>("supervisingTA1"));
        TA1Col.prefWidthProperty().bind(recTable.widthProperty().multiply(0.17));
        TA2Col = new TableColumn<>(props.getProperty(RecitationsProp.TA_COL_TEXT.toString()));
        TA2Col.setCellValueFactory(new PropertyValueFactory<>("supervisingTA2"));
        TA2Col.prefWidthProperty().bind(recTable.widthProperty().multiply(0.17));
        recTable.getColumns().add(sectionCol);
        recTable.getColumns().add(instructorCol);
        recTable.getColumns().add(dayTimeCol);
        recTable.getColumns().add(locationCol);
        recTable.getColumns().add(TA1Col);
        recTable.getColumns().add(TA2Col);
        emptyTableLabel = new Label(props.getProperty(RecitationsProp.TABLE_EMPTY_LABEL_TEXT.toString()));
        recTable.setPlaceholder(emptyTableLabel);

        String recitationsHeaderText = props.getProperty(RecitationsProp.RECITATIONS_HEADER_TEXT.toString());
        recitationsHeaderLabel = new Label(recitationsHeaderText);
        String deleteButtonText = props.getProperty(RecitationsProp.DELETE_BUTTON_TEXT.toString());
        deleteButton = new Button(deleteButtonText);
        recitationsHeaderBox = new HBox();
        recitationsHeaderBox.getChildren().add(recitationsHeaderLabel);
        recitationsHeaderBox.getChildren().add(deleteButton);

        String addEditHeaderText = props.getProperty(RecitationsProp.ADD_EDIT_HEADER_TEXT.toString());
        addEditHeaderLabel = new Label(addEditHeaderText);

        String sectionText = props.getProperty(RecitationsProp.SECTION_LABEL_TEXT.toString());
        sectionLabel = new Label(sectionText);
        String instructorText = props.getProperty(RecitationsProp.INSTRUCTOR_LABEL_TEXT.toString());
        instructorLabel = new Label(instructorText);
        String dayTimeText = props.getProperty(RecitationsProp.DAY_TIME_LABEL_TEXT.toString());
        dayTimeLabel = new Label(dayTimeText);
        String locationText = props.getProperty(RecitationsProp.LOCATION_LABEL_TEXT.toString());
        locationLabel = new Label(locationText);
        String supervisingTAText = props.getProperty(RecitationsProp.SUPERVISING_TA_LABEL_TEXT.toString());
        supervisingLabel = new Label(supervisingTAText);
        supervisingLabel2 = new Label(supervisingTAText);
        String addUpdateText = props.getProperty(RecitationsProp.ADD_UPDATE_BUTTON_TEXT.toString());
        addUpdateButton = new Button(addUpdateText);
        editingToolsPane = new GridPane();
        editingToolsPane.add(sectionLabel, 0, 0);
        editingToolsPane.add(instructorLabel, 0, 1);
        editingToolsPane.add(dayTimeLabel, 0, 2);
        editingToolsPane.add(locationLabel, 0, 3);
        editingToolsPane.add(supervisingLabel, 0, 4);
        editingToolsPane.add(supervisingLabel2, 0, 5);
        editingToolsPane.add(addUpdateButton, 0, 6);

        sectionField = new TextField();
        instructorField = new TextField();
        dayTimeField = new TextField();
        locationField = new TextField();
        supervisingCombo = new ComboBox();
        supervisingCombo2 = new ComboBox();
        String clearButtonText = props.getProperty(RecitationsProp.CLEAR_BUTTON_TEXT.toString());
        clearButton = new Button(clearButtonText);
        editingToolsPane.add(sectionField, 1, 0);
        editingToolsPane.add(instructorField, 1, 1);
        editingToolsPane.add(dayTimeField, 1, 2);
        editingToolsPane.add(locationField, 1, 3);
        editingToolsPane.add(supervisingCombo, 1, 4);
        editingToolsPane.add(supervisingCombo2, 1, 5);
        editingToolsPane.add(clearButton, 1, 6);

        addEditPane = new VBox();
        addEditPane.getChildren().add(addEditHeaderLabel);
        addEditPane.getChildren().add(editingToolsPane);

        recitationsPane = new VBox();
        recitationsPane.getChildren().add(recitationsHeaderBox);
        recitationsPane.getChildren().add(recTable);
        recitationsPane.getChildren().add(addEditPane);
        String recitationsTabName = props.getProperty(RecitationsProp.RECITATIONS_TAB_NAME.toString());
        recitationsTab = new Tab(recitationsTabName);
        recitationsTab.setClosable(false);
        recitationsTab.setContent(recitationsPane);

        controller = new RecitationsSpaceController(app);

        recTable.setOnMouseClicked(e -> {
            updateMode = controller.handleUpdateMode(e);
        });

        recTable.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.DELETE)) {
                controller.handleDeleteRec();
            }
        });

        addUpdateButton.setOnAction(e -> {
            if (!updateMode) {
                controller.handleAddRec();
            } else {
                controller.handleUpdateRec();
                updateMode =false;
            }
        });

        deleteButton.setOnAction(e -> {
            controller.handleDeleteRec();
        });
        recitationsTab.setOnSelectionChanged(e -> {
            if (recitationsTab.isSelected()) {
                recitationsTab.setStyle("-fx-background-color:#fcc99c;");
            } else {
                recitationsTab.setStyle("-fx-background-color:#ffffff;");
            }

        });
        
        clearButton.setOnAction(e->{
            controller.handleClearButton();
        });

    }

    void reloadRecWorkspace(AppDataComponent dataComponent) {
        TAData tadata = ((CSGDataComponent) dataComponent).getTaData();
        RecitationData data = ((CSGDataComponent) dataComponent).getRecData();
        recTable.getItems().clear();
        clearEdit();
        for (Recitation r : data.getRecitations()) {
            recTable.getItems().add(r);
        }
        for (TeachingAssistant ta : tadata.getTeachingAssistants()) {
            this.getSupervisingCombo().getItems().add(ta.getName());
            this.getSupervisingCombo2().getItems().add(ta.getName());
        }
    }

    public void clearEdit() {
        sectionField.clear();
        instructorField.clear();
        dayTimeField.clear();
        locationField.clear();
        supervisingCombo.getSelectionModel().clearSelection();
        supervisingCombo2.getSelectionModel().clearSelection();
    }

    public Tab getRecTab() {
        return recitationsTab;
    }

    public Tab getRecitationsTab() {
        return recitationsTab;
    }

    public VBox getRecitationsPane() {
        return recitationsPane;
    }

    public HBox getRecitationsHeaderBox() {
        return recitationsHeaderBox;
    }

    public TableView<Recitation> getRecTable() {
        return recTable;
    }

    public TableColumn<Recitation, String> getSectionCol() {
        return sectionCol;
    }

    public TableColumn<Recitation, String> getInstructorCol() {
        return instructorCol;
    }

    public TableColumn<Recitation, String> getDayTimeCol() {
        return dayTimeCol;
    }

    public TableColumn<Recitation, String> getLocationCol() {
        return locationCol;
    }

    public TableColumn<Recitation, String> getTA1Col() {
        return TA1Col;
    }

    public TableColumn<Recitation, String> getTA2Col() {
        return TA2Col;
    }

    public Label getRecitationsHeaderLabel() {
        return recitationsHeaderLabel;
    }

    public Label getAddEditHeaderLabel() {
        return addEditHeaderLabel;
    }

    public Label getDeleteButtonLabel() {
        return deleteButtonLabel;
    }

    public Label getAddUpdateButtonLabel() {
        return addUpdateButtonLabel;
    }

    public Label getClearButtonLabel() {
        return clearButtonLabel;
    }

    public Label getSectionLabel() {
        return sectionLabel;
    }

    public Label getInstructorLabel() {
        return instructorLabel;
    }

    public Label getLocationLabel() {
        return locationLabel;
    }

    public Label getDayTimeLabel() {
        return dayTimeLabel;
    }

    public Label getSupervisingLabel() {
        return supervisingLabel;
    }

    public Label getSupervisingLabel2() {
        return supervisingLabel2;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    public VBox getAddEditPane() {
        return addEditPane;
    }

    public GridPane getEditingToolsPane() {
        return editingToolsPane;
    }

    public TextField getSectionField() {
        return sectionField;
    }

    public TextField getInstructorField() {
        return instructorField;
    }

    public TextField getDayTimeField() {
        return dayTimeField;
    }

    public TextField getLocationField() {
        return locationField;
    }

    public ComboBox getSupervisingCombo() {
        return supervisingCombo;
    }

    public ComboBox getSupervisingCombo2() {
        return supervisingCombo2;
    }

    public boolean isUpdateMode() {
        return updateMode;
    }

}
