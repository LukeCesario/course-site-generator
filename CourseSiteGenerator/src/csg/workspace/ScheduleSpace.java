package csg.workspace;

import csg.CSGApplication;
import csg.Properties.ScheduleProp;
import csg.data.CSGDataComponent;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.data.ScheduleItemType;
import csg.uistate.ScheduleSpaceController;
import djf.components.AppDataComponent;
import java.time.DayOfWeek;
import java.time.LocalDate;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Luke Cesario
 */
public class ScheduleSpace {

    private Tab scheduleTab;
    private VBox scheduleBox;
    private VBox calendarBoundariesPane;
    private HBox calendarBoundsSelectorPane;
    private VBox scheduleItemsPane;
    private HBox scheduleItemsHeaderBox;
    private TableView<ScheduleItem> scheduleTable;
    private TableColumn<ScheduleItem, String> typeColumn;
    private TableColumn<ScheduleItem, String> dateColumn;
    private TableColumn<ScheduleItem, String> titleColumn;
    private TableColumn<ScheduleItem, String> topicColumn;
    private GridPane addEditPane;

    private Label scheduleHeaderLabel;
    private Label calendarBoundaryLabel;
    private Label scheduleItemsHeaderLabel;
    private Label startingMondayLabel;
    private Label endingFridayLabel;
    private Label addEditHeaderLabel;
    private Label typeLabel;
    private Label dateLabel;
    private Label timeLabel;
    private Label titleLabel;
    private Label topicLabel;
    private Label linkLabel;
    private Label criteriaLabel;
    private Label emptyTableLabel;

    private DatePicker startDatePicker;
    private DatePicker endDatePicker;
    private DatePicker recDatePicker;

    private ComboBox typeCombo;

    private TextField timeField;
    private TextField titleField;
    private TextField topicField;
    private TextField linkField;
    private TextField criteriaField;

    private Button deleteButton;
    private Button addUpdateButton;
    private Button clearButton;

    private ScheduleSpaceController controller;

    private boolean updateMode = false;
    private CSGApplication app;

    public ScheduleSpace(CSGApplication app) {
        this.app = app;

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String scheduleHeaderText = props.getProperty(ScheduleProp.SCHEDULE_HEADER_TEXT.toString());
        scheduleHeaderLabel = new Label(scheduleHeaderText);

        String calendarBoundaryText = props.getProperty(ScheduleProp.CALENDAR_BOUNDARY_TEXT.toString());
        calendarBoundaryLabel = new Label(calendarBoundaryText);
        String mondayText = props.getProperty(ScheduleProp.MONDAY_TEXT.toString());
        startingMondayLabel = new Label(mondayText);
        startDatePicker = new DatePicker();
        startDatePicker.setValue(LocalDate.now());
        String fridayText = props.getProperty(ScheduleProp.FRIDAY_TEXT.toString());
        endingFridayLabel = new Label(fridayText);
        endDatePicker = new DatePicker();
        endDatePicker.setValue(LocalDate.now());

        calendarBoundariesPane = new VBox();
        calendarBoundsSelectorPane = new HBox();
        calendarBoundariesPane.getChildren().add(calendarBoundaryLabel);
        calendarBoundsSelectorPane.getChildren().add(startingMondayLabel);
        calendarBoundsSelectorPane.getChildren().add(startDatePicker);
        calendarBoundsSelectorPane.getChildren().add(endingFridayLabel);
        calendarBoundsSelectorPane.getChildren().add(endDatePicker);
        calendarBoundariesPane.getChildren().add(calendarBoundsSelectorPane);

        String scheduleItemsHeaderText = props.getProperty(ScheduleProp.SCHEDULE_ITEMS_HEADER_TEXT.toString());
        scheduleItemsHeaderLabel = new Label(scheduleItemsHeaderText);
        String deleteButtonText = props.getProperty(ScheduleProp.DELETE_BUTTON_TEXT.toString());
        deleteButton = new Button(deleteButtonText);
        scheduleItemsHeaderBox = new HBox();
        scheduleItemsHeaderBox.getChildren().add(scheduleItemsHeaderLabel);
        scheduleItemsHeaderBox.getChildren().add(deleteButton);

        scheduleTable = new TableView<>();
        typeColumn = new TableColumn<>(props.getProperty(ScheduleProp.TYPE_COL_TEXT.toString()));
        typeColumn.setCellValueFactory(new PropertyValueFactory("type"));
        typeColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.25));
        dateColumn = new TableColumn<>(props.getProperty(ScheduleProp.DATE_COL_TEXT.toString()));
        dateColumn.setCellValueFactory(new PropertyValueFactory("formattedDate"));
        dateColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.25));
        titleColumn = new TableColumn<>(props.getProperty(ScheduleProp.TITLE_COL_TEXT.toString()));
        titleColumn.setCellValueFactory(new PropertyValueFactory("title"));
        titleColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.25));
        topicColumn = new TableColumn<>(props.getProperty(ScheduleProp.TOPIC_COL_TEXT.toString()));
        topicColumn.setCellValueFactory(new PropertyValueFactory("topic"));
        topicColumn.prefWidthProperty().bind(scheduleTable.widthProperty().multiply(0.25));
        scheduleTable.getColumns().add(typeColumn);
        scheduleTable.getColumns().add(dateColumn);
        scheduleTable.getColumns().add(titleColumn);
        scheduleTable.getColumns().add(topicColumn);
        emptyTableLabel = new Label(props.getProperty(ScheduleProp.TABLE_EMPTY_LABEL_TEXT.toString()));
        scheduleTable.setPlaceholder(emptyTableLabel);

        addEditHeaderLabel = new Label(props.getProperty(ScheduleProp.ADD_EDIT_HEADER_TEXT.toString()));
        typeLabel = new Label(props.getProperty(ScheduleProp.TYPE_LABEL_TEXT.toString()));
        typeCombo = new ComboBox();
        typeCombo.getItems().addAll(ScheduleItemType.HOLIDAY, ScheduleItemType.HW, ScheduleItemType.LECTURE,
                ScheduleItemType.RECITATION, ScheduleItemType.RECITATION, ScheduleItemType.REFERENCE);
        dateLabel = new Label(props.getProperty(ScheduleProp.DATE_LABEL_TEXT.toString()));
        recDatePicker = new DatePicker();
        recDatePicker.setValue(LocalDate.now());
        timeLabel = new Label(props.getProperty(ScheduleProp.TIME_LABEL_TEXT.toString()));
        timeField = new TextField();
        titleLabel = new Label(props.getProperty(ScheduleProp.TITLE_LABEL_TEXT.toString()));
        titleField = new TextField();
        topicLabel = new Label(props.getProperty(ScheduleProp.TOPIC_LABEL_TEXT.toString()));
        topicField = new TextField();
        linkLabel = new Label(props.getProperty(ScheduleProp.LINK_LABEL_TEXT.toString()));
        linkField = new TextField();
        criteriaLabel = new Label(props.getProperty(ScheduleProp.CRITERIA_LABEL_TEXT.toString()));
        criteriaField = new TextField();
        addUpdateButton = new Button(props.getProperty(ScheduleProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton = new Button(props.getProperty(ScheduleProp.CLEAR_BUTTON_TEXT.toString()));
        addEditPane = new GridPane();
        addEditPane.add(addEditHeaderLabel, 0, 0);
        addEditPane.add(typeLabel, 0, 1);
        addEditPane.add(typeCombo, 1, 1);
        addEditPane.add(dateLabel, 0, 2);
        addEditPane.add(recDatePicker, 1, 2);
        addEditPane.add(timeLabel, 0, 3);
        addEditPane.add(timeField, 1, 3);
        addEditPane.add(titleLabel, 0, 4);
        addEditPane.add(titleField, 1, 4);
        addEditPane.add(topicLabel, 0, 5);
        addEditPane.add(topicField, 1, 5);
        addEditPane.add(linkLabel, 0, 6);
        addEditPane.add(linkField, 1, 6);
        addEditPane.add(criteriaLabel, 0, 7);
        addEditPane.add(criteriaField, 1, 7);
        addEditPane.add(addUpdateButton, 0, 8);
        addEditPane.add(clearButton, 1, 8);

        scheduleItemsPane = new VBox();
        scheduleItemsPane.getChildren().add(scheduleItemsHeaderBox);
        scheduleItemsPane.getChildren().add(scheduleTable);
        scheduleItemsPane.getChildren().add(addEditPane);

        scheduleBox = new VBox();
        scheduleBox.getChildren().add(scheduleHeaderLabel);
        scheduleBox.getChildren().add(calendarBoundariesPane);
        scheduleBox.getChildren().add(scheduleItemsPane);
        String tabName = props.getProperty(ScheduleProp.SCHEDULE_TAB_NAME.toString());
        scheduleTab = new Tab(tabName);
        scheduleTab.setClosable(false);
        scheduleTab.setContent(scheduleBox);

        controller = new ScheduleSpaceController(app);

        scheduleTable.setOnMouseClicked(e -> {
            updateMode = controller.handleUpdateMode();
        });

        scheduleTable.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.DELETE)) {
                controller.handleDeleteItem();
            }
        });
        addUpdateButton.setOnAction(e -> {
            if (!updateMode) {
                controller.handleAddItem();
            } else {
                controller.handleUpdateItem();
                updateMode = false;
            }
        });

        deleteButton.setOnAction(e -> {
            controller.handleDeleteItem();
        });
        clearButton.setOnAction(e -> {
            controller.handleClearButton();
        });
        scheduleTab.setOnSelectionChanged(e -> {
            if (scheduleTab.isSelected()) {
                scheduleTab.setStyle("-fx-background-color:#fcc99c;");
            } else {
                scheduleTab.setStyle("-fx-background-color:#ffffff;");
            }

        });
        
        startDatePicker.setOnAction(e->{
            controller.handleStartDate();
        });
        
        endDatePicker.setOnAction(e->{
            controller.handleEndDate();
        });

    }

    void reloadSchWorkspace(AppDataComponent dataComponent) {
        scheduleTable.getItems().clear();
        ScheduleData data = ((CSGDataComponent) dataComponent).getScheduleData();
        startDatePicker.setValue(LocalDate.of(data.getStartingMondayYear(), data.getStartingMondayMonth(), data.getStartingMondayDay()));
        //startDatePicker.getValue().getDayOfWeek().adjustInto(new LocalDate(startDatePicker.getValue().getYear(), DayOfWeek.MONDAY, startDatePicker.getValue().));
        //startDatePicker.getValue().plusDays(1);
        endDatePicker.setValue(LocalDate.of(data.getEndingFridayYear(), data.getEndingFridayMonth(), data.getEndingFridayDay()));
       // while (endDatePicker.getValue().getDayOfWeek().compareTo(DayOfWeek.FRIDAY) > 0) {
            //endDatePicker.getValue().plusDays(1);
      //  }
        for (ScheduleItem h : data.getHolidayItems()) {
            scheduleTable.getItems().add(h);
        }
        for (ScheduleItem h : data.getHwItems()) {
            scheduleTable.getItems().add(h);
        }
        for (ScheduleItem l : data.getLectureItems()) {
            scheduleTable.getItems().add(l);
        }
        for (ScheduleItem r : data.getRecitationItems()) {
            scheduleTable.getItems().add(r);
        }
        for (ScheduleItem r : data.getReferenceItems()) {
            scheduleTable.getItems().add(r);
        }
    }

    public void clearEdit() {
        getTypeCombo().getSelectionModel().clearSelection();
        getRecDatePicker().getEditor().clear();
        getTitleField().clear();
        getTopicField().clear();
        getLinkField().clear();
        getCriteriaField().clear();
        getTimeField().clear();
        getScheduleTable().getSelectionModel().clearSelection();
        
    }

    public Tab getScheduleTab() {
        return scheduleTab;
    }

    public VBox getScheduleBox() {
        return scheduleBox;
    }

    public VBox getCalendarBoundariesPane() {
        return calendarBoundariesPane;
    }

    public HBox getCalendarBoundsSelectorPane() {
        return calendarBoundsSelectorPane;
    }

    public VBox getScheduleItemsPane() {
        return scheduleItemsPane;
    }

    public HBox getScheduleItemsHeaderBox() {
        return scheduleItemsHeaderBox;
    }

    public TableView<ScheduleItem> getScheduleTable() {
        return scheduleTable;
    }

    public TableColumn<ScheduleItem, String> getTypeColumn() {
        return typeColumn;
    }

    public TableColumn<ScheduleItem, String> getDateColumn() {
        return dateColumn;
    }

    public TableColumn<ScheduleItem, String> getTitleColumn() {
        return titleColumn;
    }

    public TableColumn<ScheduleItem, String> getTopicColumn() {
        return topicColumn;
    }

    public GridPane getAddEditPane() {
        return addEditPane;
    }

    public Label getScheduleHeaderLabel() {
        return scheduleHeaderLabel;
    }

    public Label getCalendarBoundaryLabel() {
        return calendarBoundaryLabel;
    }

    public Label getScheduleItemsHeaderLabel() {
        return scheduleItemsHeaderLabel;
    }

    public Label getStartingMondayLabel() {
        return startingMondayLabel;
    }

    public Label getEndingFridayLabel() {
        return endingFridayLabel;
    }

    public Label getAddEditHeaderLabel() {
        return addEditHeaderLabel;
    }

    public Label getTypeLabel() {
        return typeLabel;
    }

    public Label getDateLabel() {
        return dateLabel;
    }

    public Label getTimeLabel() {
        return timeLabel;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public Label getTopicLabel() {
        return topicLabel;
    }

    public Label getLinkLabel() {
        return linkLabel;
    }

    public Label getCriteriaLabel() {
        return criteriaLabel;
    }

    public DatePicker getStartDatePicker() {
        return startDatePicker;
    }

    public DatePicker getEndDatePicker() {
        return endDatePicker;
    }

    public DatePicker getRecDatePicker() {
        return recDatePicker;
    }

    public ComboBox getTypeCombo() {
        return typeCombo;
    }

    public TextField getTimeField() {
        return timeField;
    }

    public TextField getTitleField() {
        return titleField;
    }

    public TextField getTopicField() {
        return topicField;
    }

    public TextField getLinkField() {
        return linkField;
    }

    public TextField getCriteriaField() {
        return criteriaField;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public Button getAddUpdateButton() {
        return addUpdateButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

}
