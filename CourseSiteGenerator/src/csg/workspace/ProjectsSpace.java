package csg.workspace;

import csg.CSGApplication;
import csg.Properties.ProjectsProp;
import csg.data.CSGDataComponent;
import csg.data.ProjectData;
import csg.data.ProjectStudent;
import csg.data.ProjectTeam;
import csg.uistate.ProjectsSpaceController;
import djf.components.AppDataComponent;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;

/**
 *
 * @author Luke Cesario
 */
public class ProjectsSpace {

    private Tab projectsTab;
    private VBox projectsBox;
    private VBox teamsBox;
    private VBox studentsBox;

    private HBox teamsHeaderBox;
    private HBox studentsHeaderBox;

    private GridPane editTeamsPane;
    private GridPane editStudentsPane;

    private TableView<ProjectTeam> teamsTable;
    private TableColumn<ProjectTeam, String> nameCol;
    private TableColumn<ProjectTeam, String> colorCol;
    private TableColumn<ProjectTeam, String> textColorCol;
    private TableColumn<ProjectTeam, String> linkCol;
    private Label emptyTeamsTableLabel;

    private TableView<ProjectStudent> studentsTable;
    private TableColumn<ProjectStudent, String> firstNameCol;
    private TableColumn<ProjectStudent, String> lastNameCol;
    private TableColumn<ProjectStudent, String> teamCol;
    private TableColumn<ProjectStudent, String> roleCol;
    private Label emptyStudentsTableLabel;

    private Button deleteTeamButton;
    private Button deleteStudentButton;
    private Button addUpdateTeamButton;
    private Button addUpdateStudentButton;
    private Button clearEditTeamButton;
    private Button clearEditStudentButton;

    private ColorPicker textColorPicker;
    private ColorPicker colorPicker;

    private ComboBox teamSelector;

    private TextField nameField;
    private TextField linkField;
    private TextField firstNameField;
    private TextField lastNameField;
    private TextField roleField;

    private Label projectsHeaderLabel;
    private Label teamsHeaderLabel;
    private Label studentsHeaderLabel;
    private Label teamNameLabel;
    private Label colorLabel;
    private Label textColorLabel;
    private Label linkLabel;
    private Label addEditTeamsLabel;
    private Label addEditStudentsLabel;
    private Label firstNameLabel;
    private Label lastNameLabel;
    private Label teamNameSelectorLabel;
    private Label roleLabel;

    private CSGApplication app;
    private boolean updateTeamMode = false;
    private boolean updateStudentMode = false;
    private ProjectsSpaceController controller;

    public ProjectsSpace(CSGApplication app) {
        this.app = app;
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        projectsHeaderLabel = new Label(props.getProperty(ProjectsProp.PROJECTS_HEADER_TEXT.toString()));

        teamsHeaderLabel = new Label(props.getProperty(ProjectsProp.TEAMS_HEADER_TEXT.toString()));
        deleteTeamButton = new Button(props.getProperty(ProjectsProp.DELETE_BUTTON_TEXT.toString()));
        teamsHeaderBox = new HBox();
        teamsHeaderBox.getChildren().addAll(teamsHeaderLabel, deleteTeamButton);

        teamsTable = new TableView<>();
        nameCol = new TableColumn<>(props.getProperty(ProjectsProp.TEAM_NAME_COL_TEXT.toString()));
        nameCol.setCellValueFactory(new PropertyValueFactory("name"));
        nameCol.prefWidthProperty().bind(teamsTable.widthProperty().multiply(0.25));
        colorCol = new TableColumn<>(props.getProperty(ProjectsProp.COLOR_COL_TEXT.toString()));
        colorCol.setCellValueFactory(new PropertyValueFactory("color"));
        colorCol.prefWidthProperty().bind(teamsTable.widthProperty().multiply(0.25));
        textColorCol = new TableColumn<>(props.getProperty(ProjectsProp.TEXT_COLOR_COL_TEXT.toString()));
        textColorCol.setCellValueFactory(new PropertyValueFactory("textColor"));
        textColorCol.prefWidthProperty().bind(teamsTable.widthProperty().multiply(0.25));
        linkCol = new TableColumn<>(props.getProperty(ProjectsProp.TEAM_LINK_COL_TEXT.toString()));
        linkCol.setCellValueFactory(new PropertyValueFactory("link"));
        linkCol.prefWidthProperty().bind(teamsTable.widthProperty().multiply(0.25));
        teamsTable.getColumns().addAll(nameCol, colorCol, textColorCol, linkCol);
        emptyTeamsTableLabel = new Label(props.getProperty(ProjectsProp.TABLE_EMPTY_LABEL_TEXT.toString()));
        teamsTable.setPlaceholder(emptyTeamsTableLabel);

        addEditTeamsLabel = new Label(props.getProperty(ProjectsProp.ADD_EDIT_HEADER_TEXT.toString()));
        teamNameLabel = new Label(props.getProperty(ProjectsProp.TEAM_NAME_FIELD_LABEL_TEXT.toString()));
        colorLabel = new Label(props.getProperty(ProjectsProp.COLOR_LABEL_TEXT.toString()));
        textColorLabel = new Label(props.getProperty(ProjectsProp.TEXT_COLOR_LABEL_TEXT.toString()));
        linkLabel = new Label(props.getProperty(ProjectsProp.LINK_LABEL_TEXT.toString()));
        nameField = new TextField();
        colorPicker = new ColorPicker();
        colorPicker.setMinHeight(30);
        textColorPicker = new ColorPicker();
        textColorPicker.setMinHeight(30);
        linkField = new TextField();
        addUpdateTeamButton = new Button(props.getProperty(ProjectsProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearEditTeamButton = new Button(props.getProperty(ProjectsProp.CLEAR_BUTTON_TEXT.toString()));
        editTeamsPane = new GridPane();
        editTeamsPane.add(addEditTeamsLabel, 0, 0);
        editTeamsPane.add(teamNameLabel, 0, 1);
        editTeamsPane.add(nameField, 1, 1);
        editTeamsPane.add(colorLabel, 0, 2);
        editTeamsPane.add(colorPicker, 1, 2);
        editTeamsPane.add(textColorLabel, 2, 2);
        editTeamsPane.add(textColorPicker, 3, 2);
        editTeamsPane.add(linkLabel, 0, 3);
        editTeamsPane.add(linkField, 1, 3);
        editTeamsPane.add(addUpdateTeamButton, 0, 4);
        editTeamsPane.add(clearEditTeamButton, 1, 4);

        teamsBox = new VBox();
        teamsBox.getChildren().addAll(teamsHeaderBox, teamsTable, editTeamsPane);

        studentsHeaderLabel = new Label(props.getProperty(ProjectsProp.STUDENTS_HEADER_TEXT.toString()));
        deleteStudentButton = new Button(props.getProperty(ProjectsProp.DELETE_BUTTON_TEXT.toString()));
        studentsHeaderBox = new HBox();
        studentsHeaderBox.getChildren().addAll(studentsHeaderLabel, deleteStudentButton);

        studentsTable = new TableView<>();
        firstNameCol = new TableColumn<>(props.getProperty(ProjectsProp.FIRST_NAME_COL_TEXT.toString()));
        firstNameCol.setCellValueFactory(new PropertyValueFactory("firstName"));
        firstNameCol.prefWidthProperty().bind(studentsTable.widthProperty().multiply(0.25));
        lastNameCol = new TableColumn<>(props.getProperty(ProjectsProp.LAST_NAME_COL_TEXT.toString()));
        lastNameCol.setCellValueFactory(new PropertyValueFactory("lastName"));
        lastNameCol.prefWidthProperty().bind(studentsTable.widthProperty().multiply(0.25));
        teamCol = new TableColumn<>(props.getProperty(ProjectsProp.TEAM_COL_TEXT.toString()));
        teamCol.setCellValueFactory(new PropertyValueFactory("team"));
        teamCol.prefWidthProperty().bind(studentsTable.widthProperty().multiply(0.25));
        roleCol = new TableColumn<>(props.getProperty(ProjectsProp.ROLE_COL_TEXT.toString()));
        roleCol.setCellValueFactory(new PropertyValueFactory("role"));
        roleCol.prefWidthProperty().bind(studentsTable.widthProperty().multiply(0.25));
        studentsTable.getColumns().addAll(firstNameCol, lastNameCol, teamCol, roleCol);
        emptyStudentsTableLabel = new Label(props.getProperty(ProjectsProp.TABLE_EMPTY_LABEL_TEXT.toString()));
        studentsTable.setPlaceholder(emptyStudentsTableLabel);

        addEditStudentsLabel = new Label(props.getProperty(ProjectsProp.ADD_EDIT_HEADER_TEXT.toString()));
        firstNameLabel = new Label(props.getProperty(ProjectsProp.FIRST_NAME_LABEL_TEXT.toString()));
        lastNameLabel = new Label(props.getProperty(ProjectsProp.LAST_NAME_LABEL_TEXT.toString()));
        teamNameSelectorLabel = new Label(props.getProperty(ProjectsProp.TEAM_SELECTOR_LABEL_TEXT.toString()));
        roleLabel = new Label(props.getProperty(ProjectsProp.ROLE_LABEL_TEXT.toString()));
        firstNameField = new TextField();
        lastNameField = new TextField();
        roleField = new TextField();
        teamSelector = new ComboBox();
        addUpdateStudentButton = new Button(props.getProperty(ProjectsProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearEditStudentButton = new Button(props.getProperty(ProjectsProp.CLEAR_BUTTON_TEXT.toString()));
        editStudentsPane = new GridPane();
        editStudentsPane.add(addEditStudentsLabel, 0, 0);
        editStudentsPane.add(firstNameLabel, 0, 1);
        editStudentsPane.add(firstNameField, 1, 1);
        editStudentsPane.add(lastNameLabel, 0, 2);
        editStudentsPane.add(lastNameField, 1, 2);
        editStudentsPane.add(teamNameSelectorLabel, 0, 3);
        editStudentsPane.add(teamSelector, 1, 3);
        editStudentsPane.add(roleLabel, 0, 4);
        editStudentsPane.add(roleField, 1, 4);
        editStudentsPane.add(addUpdateStudentButton, 0, 5);
        editStudentsPane.add(clearEditStudentButton, 1, 5);

        studentsBox = new VBox();
        studentsBox.getChildren().addAll(studentsHeaderBox, studentsTable, editStudentsPane);

        projectsBox = new VBox();
        projectsBox.getChildren().add(projectsHeaderLabel);
        projectsBox.getChildren().add(teamsBox);
        projectsBox.getChildren().add(studentsBox);

        projectsTab = new Tab(props.getProperty(ProjectsProp.PROJECTS_TAB_NAME.toString()));
        projectsTab.setClosable(false);
        projectsTab.setContent(projectsBox);

        controller = new ProjectsSpaceController(app);

        teamsTable.setOnMouseClicked(e -> {
            updateTeamMode = controller.handleUpdateTeamMode();
        });

        teamsTable.setOnKeyPressed(e -> {
            controller.handleDeleteTeam();
        });

        addUpdateTeamButton.setOnAction(e -> {
            if (!updateTeamMode) {
                controller.handleAddTeam();
            } else {
                controller.handleUpdateTeam();
                updateTeamMode = false;
            }
        });

        studentsTable.setOnMouseClicked(e -> {
            updateStudentMode = controller.handleUpdateStudetMode();
        });

        studentsTable.setOnKeyPressed(e -> {
            controller.handleDeleteStudent();
        });

        addUpdateStudentButton.setOnAction(e -> {
            if (!updateStudentMode) {
                controller.handleAddStudent();
            } else {
                controller.handleUpdateStudent();
                updateStudentMode = false;
            }
        });

        deleteTeamButton.setOnAction(e -> {
            controller.handleDeleteTeam();
        });

        deleteStudentButton.setOnAction(e -> {
            controller.handleDeleteStudent();
        });
        clearEditTeamButton.setOnAction(e->{
            controller.handleClearEditTeam();
        });
        clearEditStudentButton.setOnAction(e->{
            controller.handleClearEditStudent();
        });
        
        projectsTab.setOnSelectionChanged(e -> {
            if (projectsTab.isSelected()) {
                projectsTab.setStyle("-fx-background-color:#fcc99c;");
            } else {
                projectsTab.setStyle("-fx-background-color:#ffffff;");
            }

        });

    }

    void reloadProSpace(AppDataComponent dataComponent) {
        teamsTable.getItems().clear();
        studentsTable.getItems().clear();
        clearTeamEdit();
        clearStudentEdit();
        ProjectData data = ((CSGDataComponent) dataComponent).getProjectData();
        for (ProjectTeam t : data.getTeams()) {
            teamsTable.getItems().add(t);
        }
        for (ProjectStudent s : data.getStudents()) {
            studentsTable.getItems().add(s);
        }
    }

    public void clearTeamEdit() {
        nameField.clear();
        colorPicker.setValue(Color.WHITE);
        textColorPicker.setValue(Color.WHITE);
        linkField.clear();
    }

    public void clearStudentEdit() {
        firstNameField.clear();
        lastNameField.clear();
        roleField.clear();
        teamSelector.getSelectionModel().clearSelection();
        teamSelector.setPromptText("");
    }

    public Tab getProjectsTab() {
        return projectsTab;
    }

    public VBox getProjectsBox() {
        return projectsBox;
    }

    public VBox getTeamsBox() {
        return teamsBox;
    }

    public VBox getStudentsBox() {
        return studentsBox;
    }

    public HBox getTeamsHeaderBox() {
        return teamsHeaderBox;
    }

    public HBox getStudentsHeaderBox() {
        return studentsHeaderBox;
    }

    public GridPane getEditTeamsPane() {
        return editTeamsPane;
    }

    public GridPane getEditStudentsPane() {
        return editStudentsPane;
    }

    public TableView<ProjectTeam> getTeamsTable() {
        return teamsTable;
    }

    public TableColumn<ProjectTeam, String> getNameCol() {
        return nameCol;
    }

    public TableColumn<ProjectTeam, String> getColorCol() {
        return colorCol;
    }

    public TableColumn<ProjectTeam, String> getTextColorCol() {
        return textColorCol;
    }

    public TableColumn<ProjectTeam, String> getLinkCol() {
        return linkCol;
    }

    public TableView<ProjectStudent> getStudentsTable() {
        return studentsTable;
    }

    public TableColumn<ProjectStudent, String> getFirstNameCol() {
        return firstNameCol;
    }

    public TableColumn<ProjectStudent, String> getLastNameCol() {
        return lastNameCol;
    }

    public TableColumn<ProjectStudent, String> getTeamCol() {
        return teamCol;
    }

    public TableColumn<ProjectStudent, String> getRoleCol() {
        return roleCol;
    }

    public Button getDeleteTeamButton() {
        return deleteTeamButton;
    }

    public Button getDeleteStudentButton() {
        return deleteStudentButton;
    }

    public Button getAddUpdateTeamButton() {
        return addUpdateTeamButton;
    }

    public Button getAddUpdateStudentButton() {
        return addUpdateStudentButton;
    }

    public Button getClearEditTeamButton() {
        return clearEditTeamButton;
    }

    public Button getClearEditStudentButton() {
        return clearEditStudentButton;
    }

    public ColorPicker getTextColorPicker() {
        return textColorPicker;
    }

    public ColorPicker getColorPicker() {
        return colorPicker;
    }

    public ComboBox getTeamSelector() {
        return teamSelector;
    }

    public TextField getNameField() {
        return nameField;
    }

    public TextField getLinkField() {
        return linkField;
    }

    public TextField getFirstNameField() {
        return firstNameField;
    }

    public TextField getLastNameField() {
        return lastNameField;
    }

    public TextField getRoleField() {
        return roleField;
    }

    public Label getProjectsHeaderLabel() {
        return projectsHeaderLabel;
    }

    public Label getTeamsHeaderLabel() {
        return teamsHeaderLabel;
    }

    public Label getStudentsHeaderLabel() {
        return studentsHeaderLabel;
    }

    public Label getTeamNameLabel() {
        return teamNameLabel;
    }

    public Label getColorLabel() {
        return colorLabel;
    }

    public Label getTextColorLabel() {
        return textColorLabel;
    }

    public Label getLinkLabel() {
        return linkLabel;
    }

    public Label getAddEditTeamsLabel() {
        return addEditTeamsLabel;
    }

    public Label getAddEditStudentsLabel() {
        return addEditStudentsLabel;
    }

    public Label getFirstNameLabel() {
        return firstNameLabel;
    }

    public Label getLastNameLabel() {
        return lastNameLabel;
    }

    public Label getTeamNameSelectorLabel() {
        return teamNameSelectorLabel;
    }

    public Label getRoleLabel() {
        return roleLabel;
    }

}
