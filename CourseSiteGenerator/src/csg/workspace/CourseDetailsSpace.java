    package csg.workspace;

import csg.CSGApplication;
import csg.Properties.CourseDetailsProp;
import csg.data.CSGDataComponent;
import csg.data.CourseData;
import csg.data.WebPage;
import csg.uistate.CourseDetailsSpaceController;
import djf.components.AppDataComponent;
import djf.settings.AppStartupConstants;
import java.io.File;
import java.util.Iterator;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Luke Cesario
 */
public class CourseDetailsSpace {

    private Tab courseDetailsTab;
    private VBox courseDetailsPane;
    private VBox courseInfoBox;
    private VBox siteTemplateBox;
    private VBox pageStyleBox;

    /*private HBox subjectNumberBox;
    private HBox semesterYearBox;
    private HBox titleBox;
    private HBox instructorNameBox;
    private HBox instructorHomeBox;
    private HBox exportDirBox;*/
    private HBox bannerBox;
    private HBox leftFooterBox;
    private HBox rightFooterBox;
    private HBox stylesheetBox;
    private HBox exportDirBox;

    private GridPane courseInfoSelectorsGrid;
    private GridPane courseInfoFieldsGrid;
    private GridPane editStyleGrid;

    private TableView<WebPage> sitePagesTable;
    private TableColumn<WebPage, String> navbarColumn;
    private TableColumn<WebPage, CheckBox> useColumn;
    private TableColumn<WebPage, String> fileColumn;
    private TableColumn<WebPage, String> scriptColumn;

    private ComboBox subjectCombo;
    private ComboBox numberCombo;
    private ComboBox semesterCombo;
    private ComboBox yearCombo;
    private ComboBox styleSheetCombo;

    private TextField titleField;
    private TextField instructorNameField;
    private TextField instructorHomeField;

    private Button changeExportDirButton;
    private Button selectTemplateDirButton;
    private Button changeBannerButton;
    private Button changeLeftFooterButton;
    private Button changeRightFooterButton;

    private CheckBox homeCheckBox;
    private CheckBox syllabusCheckBox;
    private CheckBox scheduleCheckBox;
    private CheckBox hwsCheckBox;
    private CheckBox projectsCheckBox;

    private Label courseInfoHeaderLabel;
    private Label siteTemplateHeaderLabel;
    private Label pageStyleHeaderLabel;
    private Label templateReccomendationLabel;
    private Label selectedDirLabel;
    private Label changeButtonLabel;
    private Label styleNoteLabel;
    private Label selectTemplateDirButtonLabel;
    private Label templateDirLabel;
    private Label subjectLabel;
    private Label numberLabel;
    private Label semesterLabel;
    private Label yearLabel;
    private Label titleLabel;
    private Label instructorHomeLabel;
    private Label instructorNameLabel;
    private Label exportLabel;
    private Label exportDirLabel;
    private Label sitePagesTableLabel;
    private Label bannerImageLabel;
    private Label leftFooterLabel;
    private Label rightFooterLabel;
    private Label cssDirLabel;
    private Label styleSheetLabel;
    private Label tableEmptyLabel;

    private ImageView banner;
    private ImageView leftFooter;
    private ImageView rightFooter;
    
    private CSGApplication app;
    
    private boolean hasTemplate =false;
    private boolean hasExportDir =false;

    public CourseDetailsSpace(CSGApplication app) {
        this.app = app;

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String courseInfoHeaderText = props.getProperty(CourseDetailsProp.COURSE_INFO_HEADER_TEXT.toString());
        courseInfoHeaderLabel = new Label(courseInfoHeaderText);
        String subjectLabelText = props.getProperty(CourseDetailsProp.SUBJECT_LABEL_TEXT.toString());
        subjectLabel = new Label(subjectLabelText);
        String numberLabelText = props.getProperty(CourseDetailsProp.NUMBER_LABEL_TEXT.toString());
        numberLabel = new Label(numberLabelText);
        subjectCombo = new ComboBox();
        numberCombo = new ComboBox();
        String semesterLabelText = props.getProperty(CourseDetailsProp.SEMESTER_LABEL_TEXT.toString());
        semesterLabel = new Label(semesterLabelText);
        String yearLabelText = props.getProperty(CourseDetailsProp.YEAR_LABEL_TEXT.toString());
        yearLabel = new Label(yearLabelText);
        semesterCombo = new ComboBox();
        yearCombo = new ComboBox();
        subjectCombo.getItems().addAll("CSE", "ISE");
        semesterCombo.getItems().addAll("Fall", "Spring", "Summer");
        numberCombo.getItems().addAll("214", "215", "219", "220", "308");
        yearCombo.getItems().addAll("2014", "2015", "2016", "2017", "2018", "2019");

        courseInfoSelectorsGrid = new GridPane();
        courseInfoSelectorsGrid.add(courseInfoHeaderLabel, 0, 0);
        courseInfoSelectorsGrid.add(subjectLabel, 0, 1);
        courseInfoSelectorsGrid.add(subjectCombo, 1, 1);
        courseInfoSelectorsGrid.add(semesterLabel, 2, 1);
        courseInfoSelectorsGrid.add(semesterCombo, 3, 1);
        courseInfoSelectorsGrid.add(numberLabel, 0, 2);
        courseInfoSelectorsGrid.add(numberCombo, 1, 2);
        courseInfoSelectorsGrid.add(yearLabel, 2, 2);
        courseInfoSelectorsGrid.add(yearCombo, 3, 2);

        int fieldsWidth = 800;
        String titleLabelText = props.getProperty(CourseDetailsProp.TITLE_LABEL_TEXT.toString());
        titleLabel = new Label(titleLabelText);
        titleField = new TextField();
        titleField.setPrefWidth(fieldsWidth);
        titleLabel.setStyle("-fx-background-color: rgba(0,0,0,0);");//fixes odd bug where background is white
        String instructorNameLabelText = props.getProperty(CourseDetailsProp.INSTRUCTOR_NAME_TEXT.toString());
        instructorNameLabel = new Label(instructorNameLabelText);
        instructorNameField = new TextField();
        instructorNameField.setPrefWidth(fieldsWidth);
        String instructorHomeLabelText = props.getProperty(CourseDetailsProp.INSTRUCTOR_HOME_TEXT.toString());
        instructorHomeLabel = new Label(instructorHomeLabelText);
        instructorHomeField = new TextField();
        instructorHomeField.setPrefWidth(fieldsWidth);
        String exportLabelText = props.getProperty(CourseDetailsProp.EXPORT_LABEL_TEXT.toString());
        exportLabel = new Label(exportLabelText);
        String exportDirLabelText = props.getProperty(CourseDetailsProp.EXPORT_DIR_LABEL_TEXT.toString());
        exportDirLabel = new Label(exportDirLabelText);
        String changeButtonText = props.getProperty(CourseDetailsProp.CHANGE_BUTTON_TEXT.toString());
        changeExportDirButton = new Button(changeButtonText);

        courseInfoFieldsGrid = new GridPane();
        courseInfoFieldsGrid.add(titleLabel, 0, 0);
        courseInfoFieldsGrid.add(titleField, 1, 0);
        courseInfoFieldsGrid.add(instructorNameLabel, 0, 1);
        courseInfoFieldsGrid.add(instructorNameField, 1, 1);
        courseInfoFieldsGrid.add(instructorHomeLabel, 0, 2);
        courseInfoFieldsGrid.add(instructorHomeField, 1, 2);
        courseInfoFieldsGrid.prefWidthProperty().bind(courseInfoSelectorsGrid.widthProperty());

        exportDirBox = new HBox();
        exportDirBox.getChildren().addAll(exportLabel, exportDirLabel, changeExportDirButton);
        exportDirBox.prefWidthProperty().bind(courseInfoSelectorsGrid.widthProperty());

        courseInfoBox = new VBox();
        courseInfoBox.getChildren().addAll(courseInfoSelectorsGrid, courseInfoFieldsGrid, exportDirBox);

        sitePagesTable = new TableView<>();
        String siteTemplateLabelText = props.getProperty(CourseDetailsProp.SITE_TEMPLATE_HEADER_TEXT.toString());
        siteTemplateHeaderLabel = new Label(siteTemplateLabelText);
        String siteTemplateNote = props.getProperty(CourseDetailsProp.SITE_TEMPLATE_NOTE_TEXT.toString());
        templateReccomendationLabel = new Label(siteTemplateNote);
        templateDirLabel = new Label();
        String selectTemplateText = props.getProperty(CourseDetailsProp.SELECT_TEMPLATE_BUTTON_TEXT.toString());
        selectTemplateDirButton = new Button(selectTemplateText);

        homeCheckBox = new CheckBox();
        homeCheckBox.setSelected(true);
        syllabusCheckBox = new CheckBox();
        syllabusCheckBox.setSelected(true);
        scheduleCheckBox = new CheckBox();
        scheduleCheckBox.setSelected(true);
        hwsCheckBox = new CheckBox();
        hwsCheckBox.setSelected(true);
        projectsCheckBox = new CheckBox();
        projectsCheckBox.setSelected(true);

        String sitePagesHeaderText = props.getProperty(CourseDetailsProp.SITE_PAGES_HEADER_TEXT.toString());
        sitePagesTableLabel = new Label(sitePagesHeaderText);
        sitePagesTable = new TableView<>();
        String useColText = props.getProperty(CourseDetailsProp.USE_COLUMN_TEXT.toString());
        useColumn = new TableColumn<>(useColText);
        useColumn.prefWidthProperty().bind(sitePagesTable.widthProperty().multiply(0.05));
        useColumn.setCellValueFactory(new PropertyValueFactory<>("use"));
        String navColText = props.getProperty(CourseDetailsProp.NAVBAR_COLUMN_TEXT.toString());
        navbarColumn = new TableColumn<>(navColText);
        navbarColumn.prefWidthProperty().bind(sitePagesTable.widthProperty().multiply(0.32));
        navbarColumn.setCellValueFactory(new PropertyValueFactory<>("navBarTitle"));
        String fileColText = props.getProperty(CourseDetailsProp.FILE_COLUMN_TEXT.toString());
        fileColumn = new TableColumn<>(fileColText);
        fileColumn.prefWidthProperty().bind(sitePagesTable.widthProperty().multiply(0.32));
        fileColumn.setCellValueFactory(new PropertyValueFactory<>("fileName"));
        String scriptColText = props.getProperty(CourseDetailsProp.SCRIPT_COLUMN_TEXT.toString());
        scriptColumn = new TableColumn<>(scriptColText);
        scriptColumn.prefWidthProperty().bind(sitePagesTable.widthProperty().multiply(0.32));
        scriptColumn.setCellValueFactory(new PropertyValueFactory<>("script"));
        sitePagesTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        sitePagesTable.getColumns().add(useColumn);
        sitePagesTable.getColumns().add(navbarColumn);
        sitePagesTable.getColumns().add(fileColumn);
        sitePagesTable.getColumns().add(scriptColumn);
        tableEmptyLabel = new Label(props.getProperty(CourseDetailsProp.TABLE_EMPTY_LABEL_TEXT.toString()));
        sitePagesTable.setPlaceholder(tableEmptyLabel);
        siteTemplateBox = new VBox();
        siteTemplateBox.getChildren().addAll(siteTemplateHeaderLabel, templateReccomendationLabel, templateDirLabel,
                selectTemplateDirButton, sitePagesTableLabel, sitePagesTable);
        sitePagesTable.prefWidthProperty().bind(siteTemplateBox.widthProperty().multiply(0.9));

        String pageStyleHeaderText = props.getProperty(CourseDetailsProp.PAGE_STYLE_HEADER_TEXT.toString());
        pageStyleHeaderLabel = new Label(pageStyleHeaderText);
        String bannerText = props.getProperty(CourseDetailsProp.BANNER_LABEL_TEXT.toString());
        bannerImageLabel = new Label(bannerText);
        banner = new ImageView();
        changeBannerButton = new Button(changeButtonText);
        String leftFooterText = props.getProperty(CourseDetailsProp.LEFT_FOOTER_TEXT.toString());
        leftFooterLabel = new Label(leftFooterText);
        leftFooter = new ImageView();
        changeLeftFooterButton = new Button(changeButtonText);
        String rightFooterText = props.getProperty(CourseDetailsProp.RIGHT_FOOTER_TEXT.toString());
        rightFooterLabel = new Label(rightFooterText);
        rightFooter = new ImageView();
        changeRightFooterButton = new Button(changeButtonText);
        String stylesheetText = props.getProperty(CourseDetailsProp.STYLESHEET_LABEL_TEXT.toString());
        styleSheetLabel = new Label(stylesheetText);
        cssDirLabel = new Label();
        styleSheetCombo = new ComboBox();
        String styleNote = props.getProperty(CourseDetailsProp.STYLESHEET_NOTE_TEXT.toString());
        styleNoteLabel = new Label(styleNote);
        editStyleGrid = new GridPane();
        editStyleGrid.add(bannerImageLabel, 0, 0);
        editStyleGrid.add(banner, 1, 0);
        editStyleGrid.add(changeBannerButton, 2, 0);
        editStyleGrid.add(leftFooterLabel, 0, 1);
        editStyleGrid.add(leftFooter, 1, 1);
        editStyleGrid.add(changeLeftFooterButton, 2, 1);
        editStyleGrid.add(rightFooterLabel, 0, 2);
        editStyleGrid.add(rightFooter, 1, 2);
        editStyleGrid.add(changeRightFooterButton, 2, 2);
        editStyleGrid.add(styleSheetLabel, 0, 3);
        editStyleGrid.add(cssDirLabel, 1, 3);
        editStyleGrid.add(styleSheetCombo, 2, 3);

        pageStyleBox = new VBox();
        pageStyleBox.getChildren().addAll(pageStyleHeaderLabel, editStyleGrid, styleNoteLabel);
        
        File dir = new File("./work/");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if(child.getName().endsWith("css"))
                    styleSheetCombo.getItems().add(child.getName());         
            }
        }

        courseDetailsPane = new VBox();
        courseDetailsPane.getChildren().add(courseInfoBox);
        courseDetailsPane.getChildren().add(siteTemplateBox);
        courseDetailsPane.getChildren().add(pageStyleBox);
        courseDetailsTab = new Tab();
        courseDetailsTab.setContent(courseDetailsPane);
        courseDetailsTab.setClosable(false);
        String cdTabName = props.getProperty(CourseDetailsProp.CD_TAB_NAME.toString());
        courseDetailsTab.setText(cdTabName);

        CourseDetailsSpaceController controller = new CourseDetailsSpaceController(app, this);

        subjectCombo.setOnAction(e -> {
            controller.handleSubjectCombo();
        });
        numberCombo.setOnAction(e -> {
            controller.handleNumberCombo();
        });
        semesterCombo.setOnAction(e -> {
            controller.handleSemesterCombo();
        });
        yearCombo.setOnAction(e -> {
            controller.handleYearCombo();
        });

        titleField.textProperty().addListener(e -> {
            controller.handleTitleField();
        });
        instructorNameField.textProperty().addListener(e -> {
            controller.handleInstructorNameField();
        });

        instructorHomeField.textProperty().addListener(e -> {
            controller.handleInstructorHomeField();
        });
        changeExportDirButton.setOnAction(e -> {
            hasExportDir = controller.handleChangeExportDirButton(e);
            readyForExport();
        });
        selectTemplateDirButton.setOnAction(e -> {
            hasTemplate = controller.handleSelectTemplateDirButton();
            readyForExport();
        });
        changeBannerButton.setOnAction(e -> {
            controller.handleChangeImageButton(banner);
        });
        changeLeftFooterButton.setOnAction(e -> {
            controller.handleChangeImageButton(leftFooter);
        });
        changeRightFooterButton.setOnAction(e -> {
            controller.handleChangeImageButton(rightFooter);
        });
        courseDetailsTab.setOnSelectionChanged(e->{
            if(courseDetailsTab.isSelected()){
            courseDetailsTab.setStyle("-fx-background-color:#fcc99c;");
            }else{
                courseDetailsTab.setStyle("-fx-background-color:#ffffff;");
            }
                
        });

    }
    
    public void readyForExport(){
        if(hasTemplate && hasExportDir){
            app.getGUI().markExport(true);
        }
        
    }

    void reloadCDWorkspace(AppDataComponent dataComponent) {
        resetEdit();
        CourseData data = ((CSGDataComponent) dataComponent).getCourseData();
        instructorNameField.setText(data.getInstructorName());
        instructorNameField.autosize();
        instructorHomeField.setText(data.getInstructorHome());
        subjectCombo.setPromptText(data.getSubject());
        semesterCombo.setPromptText(data.getSemester());
        numberCombo.setPromptText(data.getNumber() + "");
        yearCombo.setPromptText(data.getYear() + "");
        titleField.setText(data.getTitle());
        //exportDirLabel.setText(data.getExportDir());
    }
    
    public void resetEdit(){
        sitePagesTable.getItems().clear();
        exportDirLabel.setText(PropertiesManager.getPropertiesManager().getProperty(CourseDetailsProp.SITE_TEMPLATE_NOTE_TEXT.toString()));
        templateDirLabel.setText("");
        cssDirLabel.setText("");
    }

    public Tab getCourseDetailsTab() {
        return courseDetailsTab;
    }

    public VBox getCourseDetailsPane() {
        return courseDetailsPane;
    }

    public VBox getCourseInfoBox() {
        return courseInfoBox;
    }

    public VBox getSiteTemplateBox() {
        return siteTemplateBox;
    }

    public VBox getPageStyleBox() {
        return pageStyleBox;
    }

    public HBox getBannerBox() {
        return bannerBox;
    }

    public HBox getLeftFooterBox() {
        return leftFooterBox;
    }

    public HBox getRightFooterBox() {
        return rightFooterBox;
    }

    public HBox getStylesheetBox() {
        return stylesheetBox;
    }

    public GridPane getCourseInfoSelectorsGrid() {
        return courseInfoSelectorsGrid;
    }

    public GridPane getCourseInfoFieldsGrid() {
        return courseInfoFieldsGrid;
    }

    public TableView<WebPage> getSitePagesTable() {
        return sitePagesTable;
    }

    public TableColumn<WebPage, String> getNavbarColumn() {
        return navbarColumn;
    }

    public TableColumn<WebPage, CheckBox> getUseColumn() {
        return useColumn;
    }

    public TableColumn<WebPage, String> getFileColumn() {
        return fileColumn;
    }

    public TableColumn<WebPage, String> getScriptColumn() {
        return scriptColumn;
    }

    public ComboBox getSubjectCombo() {
        return subjectCombo;
    }

    public ComboBox getNumberCombo() {
        return numberCombo;
    }

    public ComboBox getSemesterCombo() {
        return semesterCombo;
    }

    public ComboBox getYearCombo() {
        return yearCombo;
    }

    public ComboBox getStyleSheetCombo() {
        return styleSheetCombo;
    }

    public TextField getTitleField() {
        return titleField;
    }

    public TextField getInstructorNameField() {
        return instructorNameField;
    }

    public TextField getInstructorHomeField() {
        return instructorHomeField;
    }

    public Button getChangeExportDirButton() {
        return changeExportDirButton;
    }

    public Button getSelectTemplateDirButton() {
        return selectTemplateDirButton;
    }

    public Button getChangeBannerButton() {
        return changeBannerButton;
    }

    public Button getChangeLeftFooterButton() {
        return changeLeftFooterButton;
    }

    public Button getChangeRightFooterButton() {
        return changeRightFooterButton;
    }

    public Label getCourseInfoHeaderLabel() {
        return courseInfoHeaderLabel;
    }

    public Label getSiteTemplateHeaderLabel() {
        return siteTemplateHeaderLabel;
    }

    public Label getPageStyleHeaderLabel() {
        return pageStyleHeaderLabel;
    }

    public Label getTemplateReccomendationLabel() {
        return templateReccomendationLabel;
    }

    public Label getSelectedDirLabel() {
        return selectedDirLabel;
    }

    public Label getChangeButtonLabel() {
        return changeButtonLabel;
    }

    public Label getStyleNoteLabel() {
        return styleNoteLabel;
    }

    public Label getSelectTemplateDirButtonLabel() {
        return selectTemplateDirButtonLabel;
    }

    public Label getSubjectLabel() {
        return subjectLabel;
    }

    public Label getNumberLabel() {
        return numberLabel;
    }

    public Label getSemesterLabel() {
        return semesterLabel;
    }

    public Label getYearLabel() {
        return yearLabel;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public Label getInstructorHomeLabel() {
        return instructorHomeLabel;
    }

    public Label getInstructorNameLabel() {
        return instructorNameLabel;
    }

    public Label getExportDirLabel() {
        return exportDirLabel;
    }

    public Label getSitePagesTableLabel() {
        return sitePagesTableLabel;
    }

    public Label getBannerImageLabel() {
        return bannerImageLabel;
    }

    public Label getLeftFooterLabel() {
        return leftFooterLabel;
    }

    public Label getRightFooterLabel() {
        return rightFooterLabel;
    }

    public Label getStyleSheetLabel() {
        return styleSheetLabel;
    }

    public HBox getExportDirBox() {
        return exportDirBox;
    }

    public GridPane getEditStyleGrid() {
        return editStyleGrid;
    }

    public Label getExportLabel() {
        return exportLabel;
    }

    public Label getTableEmptyLabel() {
        return tableEmptyLabel;
    }

    public Label getTemplateDirLabel() {
        return templateDirLabel;
    }

    public CheckBox getHomeCheckBox() {
        return homeCheckBox;
    }

    public CheckBox getSyllabusCheckBox() {
        return syllabusCheckBox;
    }

    public CheckBox getScheduleCheckBox() {
        return scheduleCheckBox;
    }

    public CheckBox getHwsCheckBox() {
        return hwsCheckBox;
    }

    public CheckBox getProjectsCheckBox() {
        return projectsCheckBox;
    }

}
