/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CSGApplication;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import jtps.jTPS;
import properties_manager.PropertiesManager;

/**
 *
 * @author Luke Cesario
 */
public class Workspace extends AppWorkspaceComponent {

    private CSGApplication app;
    private TabPane tabs;
    private CourseDetailsSpace cdSpace;
    private TADataSpace taSpace;
    private RecitationsSpace recSpace;
    private ScheduleSpace schSpace;
    private ProjectsSpace proSpace;
    private jTPS jtps;

    public Workspace(CSGApplication initApp) {

        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        jtps = new jTPS();

        cdSpace = new CourseDetailsSpace(initApp);
        Tab cdTab = cdSpace.getCourseDetailsTab();

        taSpace = new TADataSpace(app, jtps);
        Tab taTab = taSpace.getTATab();

        recSpace = new RecitationsSpace(app);
        Tab recTab = recSpace.getRecTab();

        schSpace = new ScheduleSpace(app);
        Tab schTab = schSpace.getScheduleTab();

        proSpace = new ProjectsSpace(app);
        Tab proTab = proSpace.getProjectsTab();

        tabs = new TabPane();
        tabs.getTabs().add(cdTab);
        tabs.getTabs().add(taTab);
        tabs.getTabs().add(recTab);
        tabs.getTabs().add(schTab);
        tabs.getTabs().add(proTab);

        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(tabs);

        workspace.setOnKeyPressed(e -> {
            KeyCombination ctrlZ = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN);
            KeyEvent k = (KeyEvent) e;
            if (k.getCode() == KeyCode.Z && k.isControlDown()) {
                jtps.undoTransaction();
                app.getGUI().updateToolbarControls(false);
            }
            KeyCombination ctrlY = new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN);
            if (ctrlY.match(k)) {
                jtps.doTransaction();
                app.getGUI().updateToolbarControls(false);
            }
        });
    }

    @Override
    public void resetWorkspace() {
        cdSpace.resetEdit();
        recSpace.clearEdit();
        schSpace.clearEdit();
        proSpace.clearStudentEdit();
        proSpace.clearTeamEdit();
        taSpace.resetGrid();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        //taSpace.reloadTAWorkspace(dataComponent);
        cdSpace.reloadCDWorkspace(dataComponent);
        recSpace.reloadRecWorkspace(dataComponent);
        schSpace.reloadSchWorkspace(dataComponent);
        proSpace.reloadProSpace(dataComponent);
        taSpace.reloadTASpace(dataComponent);
    }

    public TabPane getTabs() {
        return tabs;
    }

    public CourseDetailsSpace getCdSpace() {
        return cdSpace;
    }

    public TADataSpace getTaSpace() {
        return taSpace;
    }

    public RecitationsSpace getRecSpace() {
        return recSpace;
    }

    public ScheduleSpace getSchSpace() {
        return schSpace;
    }

    public ProjectsSpace getProSpace() {
        return proSpace;
    }

    public jTPS getJtps() {
        return jtps;
    }

    @Override
    public void redo() {
       jtps.doTransaction();
    }

    @Override
    public void undo() {
        jtps.undoTransaction();
    }

}
