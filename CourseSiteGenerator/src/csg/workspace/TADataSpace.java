package csg.workspace;

import csg.CSGApplication;
import csg.Properties.TATabProp;
import csg.data.CSGDataComponent;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.style.WorkspaceStyle;
import csg.uistate.TADataSpaceController;
import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import jtps.jTPS;
import properties_manager.PropertiesManager;

/**
 *
 * @author Luke Cesario
 */
public class TADataSpace {

    private boolean updateMode = false;

    private CSGApplication app;
    private CSGDataComponent data;
    private TADataSpaceController controller;

    private Tab taTab;
    private SplitPane taContentPane;

    private HBox tasHeaderBox;
    private HBox addBox;
    private HBox officeHoursHeaderBox;
    private VBox leftPane;
    private VBox rightPane;
    private Label tasHeaderLabel;
    private Label officeHoursHeaderLabel;

    private TableView<TeachingAssistant> taTable;
    private TableColumn<TeachingAssistant, String> nameColumn;
    private TableColumn<TeachingAssistant, String> emailColumn;
    private TableColumn<TeachingAssistant, TeachingAssistant> undergradColumn;
    private Label emptyTableLabel;

    private TextField nameField;
    private TextField emailField;

    private Button addButton;
    private Button clearButton;
    private Button deleteButton;

    private ComboBox startTimeCombo;
    private ComboBox endTimeCombo;
    private Label startTimeLabel;
    private Label endTimeLabel;

    private GridPane officeHoursGridPane;
    private HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    private HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    private HashMap<String, Pane> officeHoursGridTimeCellPanes;
    private HashMap<String, Pane> officeHoursGridTACellPanes;
    private HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    private HashMap<String, Label> officeHoursGridDayHeaderLabels;
    private HashMap<String, Label> officeHoursGridTimeCellLabels;
    private HashMap<String, Label> officeHoursGridTACellLabels;

    public TADataSpace(CSGApplication app, jTPS jtps) {
        this.app = app;
        data = (CSGDataComponent) app.getDataComponent();

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(TATabProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        deleteButton = new Button(props.getProperty(TATabProp.DELETE_BUTTON_TEXT.toString()));
        tasHeaderBox.getChildren().addAll(tasHeaderLabel, deleteButton);

        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        String nameColumnText = props.getProperty(TATabProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(TATabProp.EMAIL_COLUMN_TEXT.toString());
        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
        undergradColumn = new TableColumn(props.getProperty(TATabProp.UNDERGRAD_COLUMN_TEXT.toString()));
        undergradColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.33));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.33));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.33));
        undergradColumn.setCellValueFactory((CellDataFeatures<TeachingAssistant, TeachingAssistant> features) -> new ReadOnlyObjectWrapper(features.getValue()));
        undergradColumn.setCellFactory((TableColumn<TeachingAssistant, TeachingAssistant> underCol)
                -> {
            return new TableCell<TeachingAssistant, TeachingAssistant>() {
                final CheckBox under = new CheckBox();

                @Override
                public void updateItem(final TeachingAssistant ta, boolean empty) {
                    super.updateItem(ta, empty);
                    under.setOnAction(e -> {
                        controller.handleCheckBox(ta);
                    });
                }
            };
        });
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        taTable.getColumns().add(undergradColumn);
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);
        emptyTableLabel = new Label(props.getProperty(TATabProp.TABLE_EMPTY_LABEL_TEXT.toString()));
        taTable.setPlaceholder(emptyTableLabel);

        addBox = new HBox();
        addBox.setAlignment(Pos.TOP_LEFT);
        String namePromptText = props.getProperty(TATabProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(TATabProp.EMAIL_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(TATabProp.ADD_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(TATabProp.CLEAR_BUTTON_TEXT.toString());
        nameField = new TextField();
        emailField = new TextField();
        nameField.setPromptText(namePromptText);
        emailField.setPromptText(emailPromptText);
        addButton = new Button(addButtonText);
        clearButton = new Button(clearButtonText);
        nameField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.1));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(0.1));
        addBox.getChildren().addAll(nameField, emailField, addButton, clearButton);

        leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);

        startTimeCombo = new ComboBox();
        startTimeCombo.getItems().add("12:00AM");
        for (int i = 0; i < 11; i++) {
            startTimeCombo.getItems().add((i + 1) + ":00AM");
        }
        startTimeCombo.getItems().add("12:00PM");
        for (int i = 0; i < 11; i++) {
            startTimeCombo.getItems().add((i + 1) + ":00PM");
        }
        endTimeCombo = new ComboBox();
        endTimeCombo.getItems().add("12:00AM");
        for (int i = 0; i < 11; i++) {
            endTimeCombo.getItems().add((i + 1) + ":00AM");
        }
        endTimeCombo.getItems().add("12:00PM");
        for (int i = 0; i < 11; i++) {
            endTimeCombo.getItems().add((i + 1) + ":00PM");
        }
        String startTimeText = props.getProperty(TATabProp.START_TIME_TEXT.toString());
        String endTimeText = props.getProperty(TATabProp.END_TIME_TEXT.toString());
        startTimeLabel = new Label(startTimeText);
        endTimeLabel = new Label(endTimeText);

        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(TATabProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        officeHoursHeaderBox.getChildren().add(startTimeLabel);
        officeHoursHeaderBox.getChildren().add(startTimeCombo);
        officeHoursHeaderBox.getChildren().add(endTimeLabel);
        officeHoursHeaderBox.getChildren().add(endTimeCombo);

        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);

        taContentPane = new SplitPane();
        taContentPane.getItems().add(leftPane);
        taContentPane.getItems().add(rightPane);
        taTab = new Tab();
        taTab.setContent(taContentPane);
        taTab.setClosable(false);
        String taTabName = props.getProperty(TATabProp.TA_TAB_NAME.toString());
        taTab.setText(taTabName);

        controller = new TADataSpaceController(app);

        addButton.setOnAction(e -> {
            if (!updateMode) {
                controller.handleAddTA();
            } else {
                //controller.handleUpdateTA();
            }
        });
        /*
        clearButton.setOnAction(e -> {
            controller.handleClearFields();
            updateMode = false;
        });

        taTable.setOnKeyPressed(e -> {
            controller.handleDeleteTA(e);
        });

        taTable.setOnMouseClicked(e -> {
            updateMode = controller.handleUpdateMode(e);
        });
         */
        taTab.setOnSelectionChanged(e -> {
            if (taTab.isSelected()) {
                taTab.setStyle("-fx-background-color:#fcc99c;");
            } else {
                taTab.setStyle("-fx-background-color:#ffffff;");
            }

        });

    }

    void reloadTASpace(AppDataComponent dataComponent) {
        taTable.getItems().clear();
        TAData data = ((CSGDataComponent) dataComponent).getTaData();
        for (TeachingAssistant t : data.getTeachingAssistants()) {
            taTable.getItems().add(t);
        }
        resetGrid();
        reloadOfficeHoursGrid(data);
    }

    public void resetGrid() {
        officeHoursGridPane.getChildren().clear();
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }

    public void reloadOfficeHoursGrid(TAData tadata) {
        ArrayList<String> gridHeaders = data.getTaData().getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(tadata, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            tadata.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(tadata, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            tadata.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = tadata.getStartHour(); i < tadata.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(tadata, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            tadata.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(tadata, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            tadata.getCellTextProperty(col, row + 1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(tadata, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            tadata.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(tadata, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            tadata.getCellTextProperty(col, row + 1).set(buildCellText(endHour + 1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(tadata, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(tadata, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row + 1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
/*        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        }*/
        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        WorkspaceStyle style = (WorkspaceStyle) app.getStyleComponent();
        style.initOfficeHoursGridStyle();
    }

    public void reloadTAWorkspace(AppDataComponent dataComponent) {
        TAData taData = (TAData) ((CSGDataComponent) dataComponent).getTaData();

        reloadOfficeHoursGrid(taData);
    }

    public void addCellToGrid(TAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);

        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);

        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);

        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public SplitPane getTaDataPane() {
        return taContentPane;
    }

    public Tab getTATab() {
        return taTab;
    }

    public Tab getTaTab() {
        return taTab;
    }

    public SplitPane getTaContentPane() {
        return taContentPane;
    }

    public HBox getTasHeaderBox() {
        return tasHeaderBox;
    }

    public HBox getAddBox() {
        return addBox;
    }

    public HBox getOfficeHoursHeaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getTasHeaderLabel() {
        return tasHeaderLabel;
    }

    public Label getOfficeHoursHeaderLabel() {
        return officeHoursHeaderLabel;
    }

    public TableView<TeachingAssistant> getTaTable() {
        return taTable;
    }

    public TableColumn<TeachingAssistant, String> getNameColumn() {
        return nameColumn;
    }

    public TableColumn<TeachingAssistant, String> getEmailColumn() {
        return emailColumn;
    }

    public TextField getNameField() {
        return nameField;
    }

    public TextField getEmailField() {
        return emailField;
    }

    public Button getAddButton() {
        return addButton;
    }

    public ComboBox getStartTimeCombo() {
        return startTimeCombo;
    }

    public ComboBox getEndTimeCombo() {
        return endTimeCombo;
    }

    public Label getStartTimeLabel() {
        return startTimeLabel;
    }

    public Label getEndTimeLabel() {
        return endTimeLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }

    public VBox getLeftPane() {
        return leftPane;
    }

    public VBox getRightPane() {
        return rightPane;
    }

}
