/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.Recitation;
import csg.workspace.RecitationsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class ClearRecEditTransaction implements jTPS_Transaction {

    private Recitation cleared;
    private CSGApplication app;

    public ClearRecEditTransaction(Recitation cleared, CSGApplication app) {
        this.cleared = cleared;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.clearEdit();
        space.getRecTable().getSelectionModel().clearSelection();
    }

    @Override
    public void undoTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getSectionField().setText(cleared.getSection());
        space.getDayTimeField().setText(cleared.getDayAndTime());
        space.getInstructorField().setText(cleared.getInstructor());
        space.getLocationField().setText(cleared.getLocation());
        space.getSupervisingCombo().getSelectionModel().select(cleared.getSupervisingTA1());
        space.getSupervisingCombo2().getSelectionModel().select(cleared.getSupervisingTA2());
    }

}
