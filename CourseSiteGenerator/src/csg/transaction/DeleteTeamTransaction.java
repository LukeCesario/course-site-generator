/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ProjectData;
import csg.data.ProjectStudent;
import csg.data.ProjectTeam;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import java.util.ArrayList;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class DeleteTeamTransaction implements jTPS_Transaction {

    private ProjectTeam deleted;
    private CSGApplication app;
    private ArrayList<ProjectStudent> studentsRemove;

    public DeleteTeamTransaction(ProjectTeam deleted, CSGApplication app) {
        this.deleted = deleted;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getTeamsTable().getItems().remove(deleted);
        space.getTeamSelector().getItems().remove(deleted);
        data.getTeams().remove(deleted);
        studentsRemove = new ArrayList<>();
        for (ProjectStudent s : data.getStudents()) {
            if (s.getTeam().equals(deleted.getName())) {
                studentsRemove.add(s);
            }
        }
        data.getStudents().removeAll(studentsRemove);
        space.getStudentsTable().getItems().removeAll(studentsRemove);
        space.clearTeamEdit();
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getTeamsTable().getItems().add(deleted);
        space.getTeamSelector().getItems().add(deleted);

        data.getStudents().addAll(studentsRemove);
        space.getStudentsTable().getItems().addAll(studentsRemove);

    }

}
