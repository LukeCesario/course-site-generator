/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ProjectData;
import csg.data.ProjectStudent;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class UpdateStudentTransaction implements jTPS_Transaction {

    CSGApplication app;
    ProjectStudent oldStudent;
    ProjectStudent newStudent;

    public UpdateStudentTransaction(ProjectStudent oldStudent, ProjectStudent newStudent,CSGApplication app) {
        this.app = app;
        this.oldStudent = oldStudent;
        this.newStudent = newStudent;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getStudentsTable().getItems().remove(oldStudent);
        space.getStudentsTable().getItems().add(newStudent);
        data.getStudents().remove(oldStudent);
        data.getStudents().add(newStudent);
        space.clearStudentEdit();
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getStudentsTable().getItems().remove(newStudent);
        space.getStudentsTable().getItems().add(oldStudent);
        data.getStudents().remove(newStudent);
        data.getStudents().add(oldStudent);
    }

}
