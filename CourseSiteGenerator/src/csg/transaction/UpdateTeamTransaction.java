/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ProjectData;
import csg.data.ProjectTeam;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class UpdateTeamTransaction implements jTPS_Transaction {

    CSGApplication app;
    ProjectTeam oldTeam;
    ProjectTeam newTeam;

    public UpdateTeamTransaction(ProjectTeam oldTeam, ProjectTeam newTeam, CSGApplication app) {
        this.app = app;
        this.oldTeam = oldTeam;
        this.newTeam = newTeam;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getTeamsTable().getItems().remove(oldTeam);
        space.getTeamSelector().getItems().remove(oldTeam.getName());
        space.getTeamsTable().getItems().add(newTeam);
        space.getTeamSelector().getItems().add(newTeam.getName());
        data.getTeams().remove(oldTeam);
        data.getTeams().add(newTeam);
        space.clearTeamEdit();
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getTeamsTable().getItems().remove(newTeam);
        space.getTeamSelector().getItems().remove(newTeam.getName());
        space.getTeamsTable().getItems().add(oldTeam);
        space.getTeamSelector().getItems().add(oldTeam.getName());
        data.getTeams().remove(newTeam);
        data.getTeams().add(oldTeam);
    }

}
