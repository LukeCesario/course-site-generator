/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ProjectData;
import csg.data.ProjectTeam;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class AddTeamTransaction implements jTPS_Transaction{
    
    CSGApplication app;
    ProjectTeam added;

    public AddTeamTransaction(ProjectTeam added,CSGApplication app) {
        this.app = app;
        this.added = added;
    }
    
    

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        data.getTeams().add(added);
        space.getTeamsTable().getItems().add(added);
        space.getTeamSelector().getItems().add(added.getName());
        space.clearTeamEdit();
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        data.getTeams().remove(added);
        space.getTeamsTable().getItems().remove(added);
        space.getTeamSelector().getItems().remove(added.getName());
    }
    
}
