/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.workspace.RecitationsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class AddRecitationTransaction implements jTPS_Transaction {

    private Recitation recAdded;
    private CSGApplication app;

    public AddRecitationTransaction(Recitation rec, CSGApplication app) {
        recAdded = rec;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        RecitationData data = datacomp.getRecData();
        if (recAdded != null) {
            data.AddRecitation(recAdded);
        }
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getRecTable().getItems().add(recAdded);
        space.getInstructorField().clear();
        space.getDayTimeField().clear();
        space.getLocationField().clear();
        space.getSectionField().clear();
        space.getSupervisingCombo().getSelectionModel().clearSelection();
        space.getSupervisingCombo2().getSelectionModel().clearSelection();
        space.getRecTable().getSelectionModel().select(recAdded);
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        RecitationData data = datacomp.getRecData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getRecTable().getItems().remove(recAdded);
        space.getRecTable().getSelectionModel().clearSelection();
    }

}
