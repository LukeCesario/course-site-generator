/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.ProjectTeam;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class ClearTeamEditTransaction implements jTPS_Transaction {

    private ProjectTeam cleared;
    private CSGApplication app;

    public ClearTeamEditTransaction(ProjectTeam cleared,CSGApplication app) {
        this.cleared = cleared;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.clearTeamEdit();
        space.getTeamsTable().getSelectionModel().clearSelection();
    }

    @Override
    public void undoTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getNameField().setText(cleared.getName());
        space.getColorPicker().setValue(Color.web(cleared.getColor()));
        space.getTextColorPicker().setValue(Color.web(cleared.getTextColor()));
        space.getLinkField().setText(cleared.getLink());
        space.getTeamsTable().getSelectionModel().select(cleared);
    }
}
