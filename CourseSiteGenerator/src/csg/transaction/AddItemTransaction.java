/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.workspace.ScheduleSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class AddItemTransaction implements jTPS_Transaction{
    
    private CSGApplication app;
    private ScheduleItem addedItem;

    public AddItemTransaction(ScheduleItem addedItem, CSGApplication app) {
        this.app = app;
        this.addedItem = addedItem;
    }
    
    
    
    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ScheduleData data = datacomp.getScheduleData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        data.addItem(addedItem);
        space.getScheduleTable().getItems().add(addedItem);
        space.clearEdit();
        
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ScheduleData data = datacomp.getScheduleData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        space.getScheduleTable().getItems().remove(addedItem);
        data.removeItem(addedItem);
    }
    
}
