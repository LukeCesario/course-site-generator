/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.workspace.RecitationsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class UpdateRecitationTransaction implements jTPS_Transaction {

    Recitation oldRecitation;
    Recitation newRecitation;
    CSGApplication app;

    public UpdateRecitationTransaction(Recitation oldRecitation, Recitation newRecitation, CSGApplication app) {
        this.oldRecitation = oldRecitation;
        this.newRecitation = newRecitation;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        RecitationData data = datacomp.getRecData();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getRecTable().getItems().remove(oldRecitation);
        space.getRecTable().getItems().add(newRecitation);
        data.getRecitations().remove(oldRecitation);
        data.getRecitations().add(newRecitation);
    }

    @Override
    public void undoTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        RecitationData data = datacomp.getRecData();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getRecTable().getItems().remove(newRecitation);
        space.getRecTable().getItems().add(oldRecitation);
        data.getRecitations().remove(newRecitation);
        data.getRecitations().add(oldRecitation);
    }

}
