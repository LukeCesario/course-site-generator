/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.workspace.ScheduleSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class UpdateItemTransaction implements jTPS_Transaction {

    private CSGApplication app;
    private ScheduleItem oldItem;
    private ScheduleItem newItem;

    public UpdateItemTransaction(ScheduleItem oldItem, ScheduleItem newItem, CSGApplication app) {
        this.app = app;
        this.oldItem = oldItem;
        this.newItem = newItem;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ScheduleData data = datacomp.getScheduleData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        data.removeItem(oldItem);
        data.addItem(newItem);
        space.getScheduleTable().getItems().remove(oldItem);
        space.getScheduleTable().getItems().add(newItem);
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ScheduleData data = datacomp.getScheduleData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        data.removeItem(newItem);
        data.addItem(oldItem);
        space.getScheduleTable().getItems().remove(newItem);
        space.getScheduleTable().getItems().add(oldItem);
    }

}
