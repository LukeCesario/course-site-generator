
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.workspace.ScheduleSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author Luke Cesario
 */
public class DeleteItemTransaction implements jTPS_Transaction{
    
    private CSGApplication app;
    private ScheduleItem deleted;

    public DeleteItemTransaction( ScheduleItem deleted,CSGApplication app) {
        this.app = app;
        this.deleted = deleted;
    }
    
    

    @Override
    public void doTransaction() {
      CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ScheduleData data = datacomp.getScheduleData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        data.removeItem(deleted);
        space.getScheduleTable().getItems().remove(deleted);
        space.clearEdit();
        
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ScheduleData data = datacomp.getScheduleData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        data.addItem(deleted);
        space.getScheduleTable().getItems().add(deleted);
    }
    
}
