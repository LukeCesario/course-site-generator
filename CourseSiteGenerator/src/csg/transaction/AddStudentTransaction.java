/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ProjectData;
import csg.data.ProjectStudent;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class AddStudentTransaction implements jTPS_Transaction {

    private CSGApplication app;
    private ProjectStudent added;

    public AddStudentTransaction(ProjectStudent added, CSGApplication app) {
        this.app = app;
        this.added = added;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        data.getTeam(added.getTeam()).addMember(added);
        space.getStudentsTable().getItems().add(added);
        space.clearStudentEdit();
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        ProjectData data = datacomp.getProjectData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        data.getTeam(added.getTeam()).getMembers().remove(added);
        space.getStudentsTable().getItems().remove(added);
    }

}
