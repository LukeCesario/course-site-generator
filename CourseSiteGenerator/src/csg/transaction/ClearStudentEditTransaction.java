/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.ProjectStudent;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class ClearStudentEditTransaction implements jTPS_Transaction {

    private ProjectStudent cleared;
    private CSGApplication app;

    public ClearStudentEditTransaction(ProjectStudent cleared, CSGApplication app) {
        this.cleared = cleared;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.clearStudentEdit();
        space.getStudentsTable().getSelectionModel().clearSelection();
    }

    @Override
    public void undoTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        workspace.getTabs().getSelectionModel().select(space.getProjectsTab());
        space.getFirstNameField().setText(cleared.getFirstName());
        space.getLastNameField().setText(cleared.getLastName());
        space.getRoleField().setText(cleared.getRole());
        if (cleared.getTeam() == null) {
            space.getTeamSelector().getSelectionModel().clearSelection();
        } else {
            space.getTeamSelector().getSelectionModel().select(cleared.getTeam());
            space.getTeamSelector().setPromptText(cleared.getTeam());
        }
        space.getStudentsTable().getSelectionModel().select(cleared);
    }

}
