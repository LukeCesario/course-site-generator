package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.workspace.RecitationsSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author Luke Cesario
 */
public class DeleteRecitationTransaction implements jTPS_Transaction {

    Recitation deleted;
    CSGApplication app;

    public DeleteRecitationTransaction(Recitation toDelete, CSGApplication app) {
        this.deleted = toDelete;
        this.app = app;
    }

    @Override
    public void doTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        RecitationData data = datacomp.getRecData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getRecTable().getItems().remove(deleted);
        data.getRecitations().remove(deleted);
        space.clearEdit();
    }

    @Override
    public void undoTransaction() {
        CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
        RecitationData data = datacomp.getRecData();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        RecitationsSpace space = workspace.getRecSpace();
        workspace.getTabs().getSelectionModel().select(space.getRecTab());
        space.getRecTable().getItems().add(deleted);
        data.getRecitations().add(deleted);
    }

}
