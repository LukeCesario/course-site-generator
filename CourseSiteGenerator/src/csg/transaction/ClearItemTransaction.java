/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ScheduleItem;
import csg.workspace.ScheduleSpace;
import csg.workspace.Workspace;
import jtps.jTPS_Transaction;

/**
 *
 * @author onear
 */
public class ClearItemTransaction implements jTPS_Transaction {

    private ScheduleItem cleared;
    private CSGApplication app;

    public ClearItemTransaction(ScheduleItem cleared, CSGApplication app) {
        this.cleared = cleared;
        this.app = app;
    }
  

    @Override
    public void doTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        space.clearEdit();
    }

    @Override
    public void undoTransaction() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        workspace.getTabs().getSelectionModel().select(space.getScheduleTab());
        space.getTypeCombo().getSelectionModel().select(cleared.getType());
        space.getRecDatePicker().setValue(cleared.getDate());
        space.getRecDatePicker().setPromptText(cleared.getDate().toString());
        space.getTimeField().setText(cleared.getTime());
        space.getTitleField().setText(cleared.getTitle());
        space.getTopicField().setText(cleared.getTopic());
        space.getLinkField().setText(cleared.getLink());
        space.getCriteriaField().setText(cleared.getCriteria());
    }

}
