/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transaction;

import csg.CSGApplication;
import static csg.Properties.TATabProp.INVALID_TA_EMAIL;
import static csg.Properties.TATabProp.MISSING_TA_EMAIL_MESSAGE;
import static csg.Properties.TATabProp.MISSING_TA_EMAIL_TITLE;
import static csg.Properties.TATabProp.MISSING_TA_NAME_MESSAGE;
import static csg.Properties.TATabProp.MISSING_TA_NAME_TITLE;
import static csg.Properties.TATabProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static csg.Properties.TATabProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import csg.data.CSGDataComponent;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.workspace.TADataSpace;
import csg.workspace.Workspace;
import djf.ui.AppMessageDialogSingleton;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.TextField;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author onear
 */
public class AddTATransaction implements jTPS_Transaction{


        private static final String EMAIL_PATTERN
                = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        TeachingAssistant taAdded;
        CSGApplication app;
        TADataSpace space;
        private boolean defaultGrad = false;

        public AddTATransaction(String taName, String taEmail, TADataSpace space, CSGApplication app) {
            taAdded = new TeachingAssistant(taName, taEmail, defaultGrad);
            this.app = app;
            this.space = space;
        }

        /**
         * Adds a TA to the Table. Save state is set to false. Note that it must
         * first do some validation to make sure a unique name and email address
         * has been provided.
         */
        @Override
        public void doTransaction() {
            CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
            TAData data = datacomp.getTaData();
            TextField nameTextField = space.getNameField();
            TextField emailTextField = space.getEmailField();

            if (hasValidEntries(taAdded.getName(), taAdded.getEmail(), data, false)) {
                data.addTA(taAdded.getName(), taAdded.getEmail(), taAdded.getUndergrad());
                space.getTaTable().getItems().add(taAdded);
                nameTextField.setText("");
                emailTextField.setText("");
                nameTextField.requestFocus();
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                workspace.getTabs().getSelectionModel().select(space.getTATab());
                workspace.getRecSpace().getSupervisingCombo().getItems().add(taAdded.getName());
                workspace.getRecSpace().getSupervisingCombo2().getItems().add(taAdded.getName());
                app.getGUI().updateToolbarControls(false);
            }

        }

        /**
         * Removes the TA that was just added.
         */
        @Override
        public void undoTransaction() {
             CSGDataComponent datacomp = (CSGDataComponent) app.getDataComponent();
            TAData data = datacomp.getTaData();
            data.removeTA(taAdded.getName());
            space.getTaTable().getItems().remove(taAdded);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.getTabs().getSelectionModel().select(space.getTATab());
            workspace.getRecSpace().getSupervisingCombo().getItems().remove(taAdded.getName());
            workspace.getRecSpace().getSupervisingCombo2().getItems().remove(taAdded.getName());
            app.getGUI().updateToolbarControls(false);
        }

        /**
         *
         * @param name of the TA to be checked
         * @param email of the TA to be checked
         * @param data
         * @param changeable Allows the user to change one part of the TA's data
         * without changing the other
         * @return true if the entries are valid entries in this program.
         */
        private boolean hasValidEntries(String name, String email, TAData data, boolean changeable) {

            // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
            PropertiesManager props = PropertiesManager.getPropertiesManager();

            //NEEDED TO CHECK IF EMAIL IS VALID
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(email);

            // DID THE USER NEGLECT TO PROVIDE A TA NAME?
            if (name.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
                return false;
            }
            //DID THE USER NOT ENTER AN EMAIL?
            if (email.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
                return false;
            }
            //DID THE USER ENTER A VALID EMAIL ADDRESS?
            if (!isValidEmail(email)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TA_EMAIL), props.getProperty(INVALID_TA_EMAIL));
                return false;
            }
            // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
            if (data.containsTA(name, email) && !changeable) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
                return false;
            }
            return true;
        }

        /**
         * Checks to see if an email is valid
         *
         * @param email the address to be checked for proper pattern
         * @return true if email follows correct pattern otherwise false.
         */
        private boolean isValidEmail(String email) {
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        }
    }

