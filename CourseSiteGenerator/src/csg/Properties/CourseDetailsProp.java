/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.Properties;

/**
 *
 * @author Luke Cesario
 */
public enum CourseDetailsProp {
    CHANGE_BUTTON_TEXT,
    CD_TAB_NAME,
    COURSE_INFO_HEADER_TEXT,
    SUBJECT_LABEL_TEXT,
    NUMBER_LABEL_TEXT,
    SEMESTER_LABEL_TEXT,
    YEAR_LABEL_TEXT,
    TITLE_LABEL_TEXT,
    INSTRUCTOR_NAME_TEXT,
    INSTRUCTOR_HOME_TEXT,
    EXPORT_DIR_LABEL_TEXT,
    EXPORT_LABEL_TEXT,
    SITE_TEMPLATE_HEADER_TEXT,
    SITE_TEMPLATE_NOTE_TEXT,
    SELECT_TEMPLATE_BUTTON_TEXT,
    SITE_PAGES_HEADER_TEXT,
    PAGE_STYLE_HEADER_TEXT,
    BANNER_LABEL_TEXT,
    LEFT_FOOTER_TEXT,
    RIGHT_FOOTER_TEXT,
    STYLESHEET_LABEL_TEXT,
    STYLESHEET_NOTE_TEXT,
    
    USE_COLUMN_TEXT,
    NAVBAR_COLUMN_TEXT,
    FILE_COLUMN_TEXT,
    SCRIPT_COLUMN_TEXT,
    
    TABLE_EMPTY_LABEL_TEXT,
    IMAGE_SELECTOR_TITLE,
    
    IMAGE_LOADING_ERROR_TITLE,
    TROUBLE_LOADING_IMAGE_MESSAGE
    
    
    
}
