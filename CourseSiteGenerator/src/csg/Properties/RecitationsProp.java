/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.Properties;

/**
 *
 * @author Luke Cesario
 */
public enum RecitationsProp {
    RECITATIONS_HEADER_TEXT,
    ADD_EDIT_HEADER_TEXT,
    DELETE_BUTTON_TEXT,
    ADD_UPDATE_BUTTON_TEXT,
    CLEAR_BUTTON_TEXT,
    SECTION_LABEL_TEXT,
    INSTRUCTOR_LABEL_TEXT,
    LOCATION_LABEL_TEXT,
    DAY_TIME_LABEL_TEXT,
    SUPERVISING_TA_LABEL_TEXT,
    RECITATIONS_TAB_NAME,
    TABLE_EMPTY_LABEL_TEXT,
    SECTION_EXISTS_TITLE,
    SECTION_EXISTS_MESSAGE,
    EMPTY_FIELDS_TITLE, 
    EMPTY_FIELDS_MESSAGE,
    //TABLE LABELS
    SECTION_COL_TEXT,
    INSTRUCTOR_COL_TEXT,
    DAYTIME_COL_TEXT,
    LOCATION_COL_TEXT,
    TA_COL_TEXT
    
}
