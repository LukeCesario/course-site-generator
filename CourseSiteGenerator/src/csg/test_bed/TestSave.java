/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.test_bed;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.CourseData;
import csg.data.ProjectData;
import csg.data.ProjectStudent;
import csg.data.ProjectTeam;
import csg.data.Recitation;
import csg.data.RecitationData;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.data.ScheduleItemType;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.file.CSGFileComponent;
import djf.controller.AppFileController;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import javafx.collections.ObservableList;


/**
 *
 * @author onear
 */
public class TestSave {

    static CSGApplication app;

    public static void main(String... args) {
        app = new CSGApplication();
        app.loadProperties("app_properties.xml");
        CSGDataComponent data = new CSGDataComponent(app);
        
        //MODIFY COURSE DETAILS
        CourseData courseData =data.getCourseData();
        courseData.initData("CSE", "Spring", 219, 2017, "Computer Science III", "Ritwik Banerjee", "http://www3.cs.stonybrook.edu/~rbanerjee/teaching/cse219/");
        
        //add TAs
        TAData taData = data.getTaData();
        taData.addTA("Joe Shmoe", "joe.shmoe@sbu.edu", true);
        taData.addTA("Phil Gray", "phil.gray@sbu.edu", false);
       

        
        //Add a recitation
        RecitationData recData = data.getRecData();
        TeachingAssistant ta1 = new TeachingAssistant("Jane Doe", "jane.doe@sbu.edu", true);
        TeachingAssistant ta2 = new TeachingAssistant("John Shmo", "john.shmo@sbu.edu", false);
        Recitation rec1 = new Recitation("Banerjee", "R02", "Wed 3:30pm-4:23pm", "Old CS 2114",ta1, ta2);
        taData.addTA(ta1);
        taData.addTA(ta2);
        recData.AddRecitation(rec1);
        
        //Add a Schedule Item
        ScheduleData scdata = data.getScheduleData();
        LocalDate date1 = LocalDate.of(2017,1,23);
        LocalDate date2 = LocalDate.of(2017,4,23);
        ScheduleItem item = new ScheduleItem(ScheduleItemType.HOLIDAY, date1, "1:00pm", "SNOW DAY", "NA", "NA", "NA");
        ScheduleItem item2 = new ScheduleItem(ScheduleItemType.HOLIDAY, date2, "1:00pm", "FREEDOM", "NA", "NA", "NA");
        scdata.addItem(item);
        scdata.addItem(item2);
        ObservableList<ScheduleItem> holidays = scdata.getHolidayItems();
        
        //Add a Project Team
        ProjectData prodata = data.getProjectData();
        ProjectTeam team1 = new ProjectTeam("Atomic Comics", "000000", "ffffff", "http://atomicomic.com");
        ProjectStudent student = new ProjectStudent("Neal", "Beeken", team1.getName(), "leader");
        team1.addMember(student);
        prodata.addStudent(student);
        prodata.addTeam(team1);
    
        
        
        CSGFileComponent fileManager = new CSGFileComponent(app);
        File test = new File("src/csg/test_bed/SiteSaveTest.json");
        try {
            test.createNewFile();
            fileManager.saveData(data, test.getPath());
        } catch (IOException ex) {
            System.out.println("Fail");
        }
            
      

    }

}
