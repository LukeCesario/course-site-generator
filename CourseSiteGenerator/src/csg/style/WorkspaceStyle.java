/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.style;

import csg.workspace.CourseDetailsSpace;
import csg.workspace.ProjectsSpace;
import csg.workspace.RecitationsSpace;
import csg.workspace.ScheduleSpace;
import csg.workspace.TADataSpace;
import csg.workspace.Workspace;
import djf.AppTemplate;
import djf.components.AppStyleComponent;
import java.util.HashMap;
import javafx.event.EventType;
import javafx.scene.Node;

/**
 *
 * @author onear
 */
public class WorkspaceStyle extends AppStyleComponent{
    
    private AppTemplate app;
    
    private Workspace  workspace;
    
    public static String CLASS_BACK_PANE = "back_pane";
    public static String CLASS_WORKSPACE_PANE = "workspace_pane";
  
    public static String CLASS_HEADER_PANE = "header_pane";
    public static String CLASS_HEADER_LABEL = "header_label";
    
    public static String CLASS_TAB_PANE = "tab_pane";
    public static String CLASS_TAB = "tab";
    public static String CLASS_OPTIONS_GRID = "options_grid";
    
    public static String CLASS_DISPLAY_PANE = "display_pane";
    public static String CLASS_SUBHEADER = "subheader";
    
    public static String CLASS_TABLE = "table";
    public static String CLASS_TA_TABLE = "ta_table";
    public static String CLASS_TABLE_COLUMN = "table_column";
    public static String CLASS_TABLE_COLUMN_LABEL = "table_column_label";
    
    public static String CLASS_BUTTON = "button";
    public static String CLASS_CHECK_BOX = "check_box";
    public static String CLASS_COMMON_LABEL = "common_label";
    public static String CLASS_NOTE_LABEL = "note_label";
    
    public static String CLASS_TEXT_FIELD = "text_field";
    public static String CLASS_COLOR_PICKER = "color_picker";
    public static String CLASS_DATE_PICKER = "date_picker";
    public static String CLASS_SELECTOR = "selector";
    
    public static String CLASS_OFFICE_HOURS_GRID = "office_hours_grid";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE = "office_hours_grid_time_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL = "office_hours_grid_time_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE = "office_hours_grid_day_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL = "office_hours_grid_day_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE = "office_hours_grid_time_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL = "office_hours_grid_time_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE = "office_hours_grid_ta_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL = "office_hours_grid_ta_cell_label";
    
    public WorkspaceStyle(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        workspace = (Workspace) app.getWorkspaceComponent();
        // LET'S USE THE DEFAULT STYLESHEET SETUP
        super.initStylesheet(app);

        // INIT THE STYLE FOR THE FILE TOOLBAR
        app.getGUI().initFileToolbarStyle();

        // AND NOW OUR WORKSPACE STYLE
        initWorkspaceStyle();
    }

    private void initWorkspaceStyle() {
        workspace.getWorkspace().getStyleClass().add(CLASS_WORKSPACE_PANE);
        workspace.getTabs().getStyleClass().add(CLASS_TAB_PANE);
        initCourseDetailsSpaceStyle();
        initTADataSpaceStyle();
        initRectitationsSpaceStyle();
        initScheduleSpaceStyle();
        initProjectSpaceStyle();
        
    }
    
    private void initCourseDetailsSpaceStyle(){
        CourseDetailsSpace cd = workspace.getCdSpace();
        cd.getCourseDetailsTab().getStyleClass().add(CLASS_TAB);
        cd.getTitleLabel().getStyleClass().add(CLASS_TAB);
        cd.getCourseDetailsPane().getStyleClass().add(CLASS_BACK_PANE);
        cd.getCourseInfoBox().getStyleClass().add(CLASS_DISPLAY_PANE);
        
        cd.getSiteTemplateBox().getStyleClass().add(CLASS_DISPLAY_PANE);
        cd.getPageStyleBox().getStyleClass().add(CLASS_DISPLAY_PANE);
        cd.getCourseInfoHeaderLabel().getStyleClass().add(CLASS_SUBHEADER);
        cd.getSiteTemplateHeaderLabel().getStyleClass().add(CLASS_SUBHEADER);
        cd.getPageStyleHeaderLabel().getStyleClass().add(CLASS_SUBHEADER);
    }
    
    private void initTADataSpaceStyle(){
        TADataSpace ts = workspace.getTaSpace();
        ts.getTaContentPane().getStyleClass().add(CLASS_BACK_PANE);
        ts.getTaTable().getStyleClass().add(CLASS_TA_TABLE);
       
        ts.getTasHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        ts.getOfficeHoursHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        ts.getTasHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        ts.getOfficeHoursHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
    }
    
    private void initRectitationsSpaceStyle(){
        RecitationsSpace rs = workspace.getRecSpace();
        rs.getRecTab().getStyleClass().add(CLASS_TAB);
        rs.getRecitationsPane().getStyleClass().add(CLASS_BACK_PANE);
        rs.getAddEditPane().getStyleClass().add(CLASS_DISPLAY_PANE);
        rs.getRecitationsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
       
    }
    
    private void initScheduleSpaceStyle(){
        ScheduleSpace ss = workspace.getSchSpace();
        ss.getScheduleTab().getStyleClass().add(CLASS_TAB);
        ss.getScheduleBox().getStyleClass().add(CLASS_BACK_PANE);
        ss.getCalendarBoundariesPane().getStyleClass().add(CLASS_DISPLAY_PANE);
        ss.getScheduleItemsPane().getStyleClass().add(CLASS_DISPLAY_PANE);
        ss.getScheduleHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
    }
    
    private void initProjectSpaceStyle(){
        ProjectsSpace ps = workspace.getProSpace();
        ps.getProjectsTab().getStyleClass().add(CLASS_TAB);
        ps.getProjectsBox().getStyleClass().add(CLASS_BACK_PANE);
        ps.getTeamsBox().getStyleClass().add(CLASS_DISPLAY_PANE);
        ps.getStudentsBox().getStyleClass().add(CLASS_DISPLAY_PANE);
        ps.getProjectsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
    }

    public void initOfficeHoursGridStyle() {
        // RIGHT SIDE - THE OFFICE HOURS GRID TIME HEADERS
        TADataSpace taSpace = ((Workspace)app.getWorkspaceComponent()).getTaSpace();
        taSpace.getOfficeHoursGridPane().getStyleClass().add(CLASS_OFFICE_HOURS_GRID);
        setStyleClassOnAll(taSpace.getOfficeHoursGridTimeHeaderPanes(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE);
        setStyleClassOnAll(taSpace.getOfficeHoursGridTimeHeaderLabels(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(taSpace.getOfficeHoursGridDayHeaderPanes(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        setStyleClassOnAll(taSpace.getOfficeHoursGridDayHeaderLabels(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(taSpace.getOfficeHoursGridTimeCellPanes(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
        setStyleClassOnAll(taSpace.getOfficeHoursGridTimeCellLabels(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL);
        setStyleClassOnAll(taSpace.getOfficeHoursGridTACellPanes(), CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        setStyleClassOnAll(taSpace.getOfficeHoursGridTACellLabels(), CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL);
        //taSpace.getNewEndBox().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_NEW_END_TIME); 
        //taSpace.getNewStartBox().getStyleClass().add(CLASS_OFFICE_HOURS_GRID_NEW_START_TIME);
        //taSpace.getChangeTimeButton().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON); 
    }
    
    /**
     * This helper method initializes the style of all the nodes in the nodes
     * map to a common style, styleClass.
     */
    private void setStyleClassOnAll(HashMap nodes, String styleClass) {
        for (Object nodeObject : nodes.values()) {
            Node n = (Node)nodeObject;
            n.getStyleClass().add(styleClass);
        }
    }
}
