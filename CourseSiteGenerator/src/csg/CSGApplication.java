package csg;

import csg.data.CSGDataComponent;
import csg.file.CSGFileComponent;
import csg.style.WorkspaceStyle;
import csg.workspace.Workspace;
import java.util.Locale;
import djf.AppTemplate;
import static javafx.application.Application.launch;

/**
 * 
 * @author Luke Cesario
 * @version 1.0
 */
public class CSGApplication extends AppTemplate {
    /**
     * This hook method must initialize all four components in the
     * proper order ensuring proper dependencies are respected, meaning
     * all proper objects are already constructed when they are needed
     * for use, since some may need others for initialization.
     */
    @Override
    public void buildAppComponentsHook() {
        fileComponent = new CSGFileComponent(this);
        dataComponent = new CSGDataComponent(this);
        workspaceComponent = new Workspace(this);
        styleComponent  = new WorkspaceStyle(this);
    }
    
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     */
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
}
