/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGApplication;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Luke Cesario
 */
public class RecitationData {
    private ObservableList<Recitation> recitations;
    private CSGApplication app;
    
    public RecitationData(CSGApplication app){
        this.app = app;
        recitations = FXCollections.observableArrayList();
    }
    
    void reset() {
        recitations.clear();
    }
    
    public void RemoveRecitation(Recitation rec){
        recitations.remove(rec);
    }
    
    public void AddRecitation(Recitation rec){
        recitations.add(rec);
    }
    
    public ObservableList<Recitation> getRecitations(){
        return recitations;
    }

    
}
