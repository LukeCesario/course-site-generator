/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGApplication;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Luke Cesario
 */
public class ProjectData {
    
    private ObservableList<ProjectTeam> teams;
    private ObservableList<ProjectStudent> students;
    private CSGApplication app;
    
    public ProjectData(CSGApplication initApp){
        app = initApp;
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();
    }
    
    void reset() {
        teams.clear();
        students.clear();
    }
    
    public ProjectTeam getTeam(String name){
        for(ProjectTeam team : teams){
            if(team.getName().equals(name)){
                return team;
            }
        }
        return null;
    }
    
    public void removeTeam(ProjectTeam team){
        teams.remove(team);
    }
    
    public void addTeam(ProjectTeam team){
        teams.add(team);
    }
    
    public void removeStudent(ProjectStudent student){
        students.remove(student);  
    }
    
    public void addStudent(ProjectStudent student){
        students.add(student);
    }

    public ObservableList<ProjectTeam> getTeams() {
        return teams;
    }

    public ObservableList<ProjectStudent> getStudents() {
        return students;
    }

    public ProjectStudent getStudent(String name){
        String firstName = name.substring(0,name.indexOf(" "));
        String lastName = name.substring(name.indexOf(" ")+1);
        for(ProjectStudent student :students){
            if(student.getFirstName().equals(firstName)&&student.getLastName().equals(lastName))
                    return student;
        }
        return null;
    }

   
    
}
