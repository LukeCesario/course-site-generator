/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Luke Cesario
 */
public class ScheduleItem {

    
    private ScheduleItemType type;
    private LocalDate date;
    private String formattedDate;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;

    public ScheduleItem(ScheduleItemType type, LocalDate date, String time, String title, String topic, String link, String criteria) {
        this.type = type;
        this.date = date;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
        if(date!=null)
            formattedDate = date.getMonthValue()+"/"+date.getDayOfMonth()+"/"+date.getYear();
    }

    

    public ScheduleItemType getType() {
        return type;
    }

    public LocalDate getDate() {
        return date;
    }
    
    public int getMonth(){
        return date.getMonthValue();
    }
    
    public int getDay(){
        return date.getDayOfMonth();
    }
    
    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public String getTopic() {
        return topic;
    }

    public String getLink() {
        return link;
    }

    public String getCriteria() {
        return criteria;
    }
    
    public String getFormattedDate(){
        return formattedDate;
    }
//    


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ScheduleItem other = (ScheduleItem) obj;
        if (!Objects.equals(this.time, other.time)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.topic, other.topic)) {
            return false;
        }
        if (!Objects.equals(this.link, other.link)) {
            return false;
        }
        if (!Objects.equals(this.criteria, other.criteria)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }
}
