/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGApplication;
import djf.components.AppDataComponent;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author onear
 */
public class CSGDataComponent implements AppDataComponent{
    
    private CSGApplication app;
    private TAData taData;
    private CourseData courseData;
    private RecitationData recData;
    private ScheduleData scheduleData;
    private ProjectData projectData;
    private String templateDir;
    
    public CSGDataComponent(CSGApplication initApp){
        app = initApp;
        courseData = new CourseData();
        taData = new TAData(app);
        recData = new RecitationData(app);
        scheduleData = new ScheduleData(app);
        projectData = new ProjectData(app);
        templateDir = "";
    }
    
    @Override
    public void resetData() {
      courseData.reset();
      taData.resetTAData();
      recData.reset();
      scheduleData.reset();
      projectData.reset();
      templateDir = "";
    }
    

    
    public TAData getTaData(){
        return taData;
    }

    public CourseData getCourseData() {
        return courseData;
    }

    public RecitationData getRecData() {
        return recData;
    }

    public ScheduleData getScheduleData() {
        return scheduleData;
    }

    public ProjectData getProjectData() {
        return projectData;
    }

    public String getTemplateDir() {
        return templateDir;
    }

    public void setTemplateDir(String templateDir) {
        this.templateDir = templateDir;
    }
    
    
    
}
