package csg.data;

import java.net.URL;
import java.util.Objects;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Luke Cesario
 */
public class ProjectTeam {
    private String name;
    private String color;
    private String textColor;
    private String link;

    private ObservableList<ProjectStudent> members;
    
    public ProjectTeam(String name, String color, String textColor, String link) {
        this.name = name;
        this.color = color;
        this.textColor = textColor;
        this.link = link;
        members = FXCollections.observableArrayList();
    }
    
       
    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getTextColor() {
        return textColor;
    }

    public String getLink() {
        return link;
    }

    public ObservableList<ProjectStudent> getMembers() {
        return members;
    }
    
    public void addMember(ProjectStudent student){
        members.add(student);
    }
    
    public ProjectStudent getMemeber(String name){
        String firstName = name.substring(0,name.indexOf(" "));
        String lastName = name.substring(name.indexOf(" ")+1);
        for(ProjectStudent student :members){
            if(student.getFirstName().equals(firstName)&&student.getLastName().equals(lastName))
                    return student;
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProjectTeam other = (ProjectTeam) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        if (!Objects.equals(this.textColor, other.textColor)) {
            return false;
        }
        if (!Objects.equals(this.link, other.link)) {
            return false;
        }
        if (!Objects.equals(this.members, other.members)) {
            return false;
        }
        return true;
    }
    
    
    
}
