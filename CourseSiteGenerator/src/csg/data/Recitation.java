package csg.data;

import java.util.Objects;

/**
 *
 * @author Luke Cesario
 */
public class Recitation {

    private String instructor;
    private String section;
    private String dayAndTime;
    private String location;
    private TeachingAssistant supervisingTA1;
    private TeachingAssistant supervisingTA2;

    public Recitation(String instructor, String section, String dayAndTime, String location, TeachingAssistant supervisingTA1, TeachingAssistant supervisingTA2) {
        this.instructor = instructor;
        this.section = section;
        this.dayAndTime = dayAndTime;
        this.location = location;
        this.supervisingTA1 = supervisingTA1;
        this.supervisingTA2 = supervisingTA2;
    }

    public String getInstructor() {
        return instructor;
    }

    public String getSection() {
        return section;
    }

    public String getDayAndTime() {
        return dayAndTime;
    }

    public String getLocation() {
        return location;
    }

    public TeachingAssistant getSupervisingTA1() {
        return supervisingTA1;
    }

    public TeachingAssistant getSupervisingTA2() {
        return supervisingTA2;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Recitation other = (Recitation) obj;
        if (!Objects.equals(this.instructor, other.instructor)) {
            return false;
        }
        if (!Objects.equals(this.section, other.section)) {
            return false;
        }
        if (!Objects.equals(this.dayAndTime, other.dayAndTime)) {
            return false;
        }
        if (!Objects.equals(this.supervisingTA1, other.supervisingTA1)) {
            return false;
        }
        if (!Objects.equals(this.supervisingTA2, other.supervisingTA2)) {
            return false;
        }
        return true;
    }

}
