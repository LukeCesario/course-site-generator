/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.scene.control.CheckBox;

/**
 *
 * @author Luke Cesario
 */
public class WebPage {

    private CheckBox use;
    private String navBarTitle;
    private String fileName;
    private String script;

    public WebPage(CheckBox use, String navBarTitle, String fileName, String script) {
        this.use = use;
        this.navBarTitle = navBarTitle;
        this.fileName = fileName;
        this.script = script;
    }

    public CheckBox getUse() {
        return use;
    }

    public String getNavBarTitle() {
        return navBarTitle;
    }

    public void setNavBarTitle(String navBarTitle) {
        this.navBarTitle = navBarTitle;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

}
