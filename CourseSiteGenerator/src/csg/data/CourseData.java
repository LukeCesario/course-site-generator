/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import java.net.URL;



/**
 *
 * @author onear
 */
public class CourseData {
    
    private String subject;
    private String semester;
    private int number;
    private int year;
    private String title;
    private String instructorName;
    private String instructorHome;
    private String exportDir;

    public CourseData() {
        this.subject = "";
        this.semester = "";
        this.number = 0;
        this.year = 0;
        this.title = "";
        this.instructorName = "";
        this.instructorHome = "";
        this.exportDir = "";
    }
    
    public void initData(String subject, String semester, int number, int year, String title, String instructorName, String instructorHome){
        this.subject = subject;
        this.semester = semester;
        this.number = number;
        this.year = year;
        this.title = title;
        this.instructorName = instructorName;
        this.instructorHome = instructorHome;
    }
    
    void reset() {
        this.subject = "";
        this.semester = "";
        this.number = 0;
        this.year = 0;
        this.title = "";
        this.instructorName = "";
        this.instructorHome = "";
        this.exportDir = "";
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public void setInstructorHome(String instructorHome) {
        this.instructorHome = instructorHome;
    }

    public void setExportDir(String exportDir) {
        this.exportDir = exportDir;
    }
    
    

    public String getSubject() {
        return subject;
    }

    public String getSemester() {
        return semester;
    }

    public int getNumber() {
        return number;
    }

    public int getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public String getInstructorHome() {
        return instructorHome;
    }

    public String getExportDir() {
        return exportDir;
    }

    
    
    
    
}
