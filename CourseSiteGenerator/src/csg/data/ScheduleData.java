/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CSGApplication;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Luke Cesario
 */
public class ScheduleData {

    private CSGApplication app;

    private int startingMondayMonth;
    private int startingMondayDay;
    private int startingMondayYear;
    private int endingFridayMonth;
    private int endingFridayDay;
    private int endingFridayYear;
    private ObservableList<ScheduleItem> holidayItems;
    private ObservableList<ScheduleItem> hwItems;
    private ObservableList<ScheduleItem> lectureItems;
    private ObservableList<ScheduleItem> recitationItems;
    private ObservableList<ScheduleItem> referenceItems;

    public ScheduleData(CSGApplication initApp) {
        app = initApp;
        holidayItems = FXCollections.observableArrayList();
        hwItems = FXCollections.observableArrayList();
        lectureItems = FXCollections.observableArrayList();
        recitationItems = FXCollections.observableArrayList();
        referenceItems = FXCollections.observableArrayList();
        startingMondayMonth = 1;
        startingMondayDay = 1;
        endingFridayMonth = 12;
        endingFridayDay = 31;
        startingMondayYear = LocalDate.now().getYear();
        endingFridayYear = LocalDate.now().getYear();

    }

    public void removeItem(ScheduleItem item) {
        if (item.getType().toString().equals(ScheduleItemType.HOLIDAY.toString())) {
            holidayItems.remove(item);
        } else if (item.getType().toString().equals(ScheduleItemType.LECTURE.toString())) {
            lectureItems.remove(item);
        } else if (item.getType().toString().equals(ScheduleItemType.HW.toString())) {
            hwItems.remove(item);
        } else if (item.getType().toString().equals(ScheduleItemType.RECITATION.toString())) {
            recitationItems.remove(item);
        } else if (item.getType().toString().equals(ScheduleItemType.REFERENCE.toString())) {
            referenceItems.remove(item);
        }
    }

    public void addItem(ScheduleItem item) {
        if (item.getType().toString().equals(ScheduleItemType.HOLIDAY.toString())) {
            holidayItems.add(item);
        } else if (item.getType().toString().equals(ScheduleItemType.LECTURE.toString())) {
            lectureItems.add(item);
        } else if (item.getType().toString().equals(ScheduleItemType.HW.toString())) {
            hwItems.add(item);
        } else if (item.getType().toString().equals(ScheduleItemType.RECITATION.toString())) {
            recitationItems.add(item);
        } else if (item.getType().toString().equals(ScheduleItemType.REFERENCE.toString())) {
            referenceItems.add(item);
        }
    }

    void reset() {
        holidayItems.clear();
        hwItems.clear();
        lectureItems.clear();
        recitationItems.clear();
        referenceItems.clear();
        startingMondayMonth = 1;
        startingMondayDay = 1;
        endingFridayMonth = 12;
        endingFridayDay = 31;
        startingMondayYear = LocalDate.now().getYear();
        endingFridayYear = LocalDate.now().getYear();
    }

    public int getStartingMondayMonth() {
        return startingMondayMonth;
    }

    public int getStartingMondayDay() {
        return startingMondayDay;
    }

    public int getEndingFridayMonth() {
        return endingFridayMonth;
    }

    public int getEndingFridayDay() {
        return endingFridayDay;
    }

    public int getStartingMondayYear() {
        return startingMondayYear;
    }

    public int getEndingFridayYear() {
        return endingFridayYear;
    }
    
    

    public ObservableList<ScheduleItem> getHolidayItems() {
        return holidayItems;
    }

    public ObservableList<ScheduleItem> getHwItems() {
        return hwItems;
    }

    public ObservableList<ScheduleItem> getLectureItems() {
        return lectureItems;
    }

    public ObservableList<ScheduleItem> getRecitationItems() {
        return recitationItems;
    }

    public ObservableList<ScheduleItem> getReferenceItems() {
        return referenceItems;
    }

    public void setStartingMondayMonth(int startingMondayMonth) {
        this.startingMondayMonth = startingMondayMonth;
    }

    public void setStartingMondayDay(int startingMondayDay) {
        this.startingMondayDay = startingMondayDay;
    }

    public void setStartingMondayYear(int startingMondayYear) {
        this.startingMondayYear = startingMondayYear;
    }

    public void setEndingFridayMonth(int endingFridayMonth) {
        this.endingFridayMonth = endingFridayMonth;
    }

    public void setEndingFridayDay(int endingFridayDay) {
        this.endingFridayDay = endingFridayDay;
    }

    public void setEndingFridayYear(int endingFridayYear) {
        this.endingFridayYear = endingFridayYear;
    }
    
    
}
