package csg.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author Richard McKenna,Luke Cesario
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>{
     // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    private final BooleanProperty undergrad;


    /**
     * Constructor initializes both the TA name and email.
     */
    public TeachingAssistant(String initName, String initEmail, boolean initUndergrad) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        undergrad = new SimpleBooleanProperty(initUndergrad);
        undergrad.set(initUndergrad);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String initEmail) {
        email.set(initEmail);
    }

    public boolean getUndergrad() {
        return undergrad.get();
    }
    
    public void setUndergrad(boolean isundergrad){
        undergrad.set(isundergrad);
    }
    
 

    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(!(o instanceof TeachingAssistant)) return false;
        TeachingAssistant ta = (TeachingAssistant) o;
        if(ta.getName().equals(name.get())&&ta.getEmail().equals(email.get())&&ta.getUndergrad()==undergrad.get())
          return true;
        return false;          
    }
}
