/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.uistate;

import csg.CSGApplication;
import csg.Properties.RecitationsProp;
import csg.data.CSGDataComponent;
import csg.data.Recitation;
import csg.data.TeachingAssistant;
import csg.transaction.AddRecitationTransaction;
import csg.transaction.ClearRecEditTransaction;
import csg.transaction.DeleteRecitationTransaction;
import csg.transaction.UpdateRecitationTransaction;
import csg.workspace.RecitationsSpace;
import csg.workspace.Workspace;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import properties_manager.PropertiesManager;

/**
 *
 * @author onear
 */
public class RecitationsSpaceController {
    
    CSGApplication app;
    Workspace workspace;
    RecitationsSpace space;
    CSGDataComponent data;
    
    public RecitationsSpaceController(CSGApplication app) {
        this.app = app;
        
    }
    
    public boolean handleUpdateMode(MouseEvent e) {
        workspace = (Workspace) app.getWorkspaceComponent();
        space = workspace.getRecSpace();
        data = (CSGDataComponent) app.getDataComponent();
        TableView recTable = space.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return false;
        }
        Recitation rec = (Recitation) selectedItem;
        space.getSectionField().setText(rec.getSection());
        space.getInstructorField().setText(rec.getInstructor());
        space.getDayTimeField().setText(rec.getDayAndTime());
        space.getLocationField().setText(rec.getLocation());
        if (rec.getSupervisingTA1() == null) {
            space.getSupervisingCombo().setPromptText("");
        } else {
            space.getSupervisingCombo().setPromptText(rec.getSupervisingTA1().getName());
        }
        if (rec.getSupervisingTA2() == null) {
            space.getSupervisingCombo2().setPromptText("");
        } else {
            space.getSupervisingCombo2().setPromptText(rec.getSupervisingTA2().getName());
        }
        return true;
    }
    
    public void handleAddRec() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        workspace = (Workspace) app.getWorkspaceComponent();
        space = workspace.getRecSpace();
        data = (CSGDataComponent) app.getDataComponent();
        String section = space.getSectionField().getText();
        for (Recitation r : data.getRecData().getRecitations()) {
            if (r.getSection().equals(section)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(RecitationsProp.SECTION_EXISTS_TITLE), props.getProperty(RecitationsProp.SECTION_EXISTS_MESSAGE));
                return;
            }
        }
        String instructor = space.getInstructorField().getText();
        String dayTime = space.getDayTimeField().getText();
        String location = space.getLocationField().getText();
        if (section == null || section.isEmpty() || instructor == null || instructor.isEmpty() || dayTime == null || dayTime.isEmpty() || location == null || location.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(RecitationsProp.EMPTY_FIELDS_TITLE), props.getProperty(RecitationsProp.EMPTY_FIELDS_MESSAGE));
        }
        String ta1 = (String) space.getSupervisingCombo().getSelectionModel().getSelectedItem();
        String ta2 = (String) space.getSupervisingCombo2().getSelectionModel().getSelectedItem();
        if (ta1 == null) {
            ta1 = "";
        }
        if (ta2 == null) {
            ta2 = "";
        }
        Recitation rec = new Recitation(instructor, section, dayTime, location, data.getTaData().getTA(ta1), data.getTaData().getTA(ta2));
        AddRecitationTransaction t = new AddRecitationTransaction(rec, app);
        workspace.getJtps().addTransaction(t);
        space.getRecTable().sort();
        app.getGUI().updateToolbarControls(false);
    }
    
    public void handleUpdateRec() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        workspace = (Workspace) app.getWorkspaceComponent();
        space = workspace.getRecSpace();
        data = (CSGDataComponent) app.getDataComponent();
        String section = space.getSectionField().getText();
        String instructor = space.getInstructorField().getText();
        String dayTime = space.getDayTimeField().getText();
        String location = space.getLocationField().getText();
        String ta1 = (String) space.getSupervisingCombo().getSelectionModel().getSelectedItem();
        String ta2 = (String) space.getSupervisingCombo2().getSelectionModel().getSelectedItem();
        if (section == null || section.isEmpty() || instructor == null || instructor.isEmpty() || dayTime == null || dayTime.isEmpty() || location == null || location.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(RecitationsProp.EMPTY_FIELDS_TITLE), props.getProperty(RecitationsProp.EMPTY_FIELDS_MESSAGE));
        }
        if (ta1 == null) {
            ta1 = "";
        }
        if (ta2 == null) {
            ta2 = "";
        }
        Recitation rec = new Recitation(instructor, section, dayTime, location, data.getTaData().getTA(ta1), data.getTaData().getTA(ta2));
        UpdateRecitationTransaction t = new UpdateRecitationTransaction(space.getRecTable().getSelectionModel().getSelectedItem(), rec, app);
        workspace.getJtps().addTransaction(t);
        space.getRecTable().sort();
        app.getGUI().updateToolbarControls(false);
    }
    
    public void handleDeleteRec() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        workspace = (Workspace) app.getWorkspaceComponent();
        space = workspace.getRecSpace();
        data = (CSGDataComponent) app.getDataComponent();
        Recitation selected = space.getRecTable().getSelectionModel().getSelectedItem();
        if (selected == null) {
            return;
        }
        DeleteRecitationTransaction t = new DeleteRecitationTransaction(selected, app);
        workspace.getJtps().addTransaction(t);
        app.getGUI().updateToolbarControls(false);
    }

    public void handleClearButton() {
        workspace = (Workspace) app.getWorkspaceComponent();
        space = workspace.getRecSpace();
        data = (CSGDataComponent) app.getDataComponent();
        Recitation selected = (Recitation) space.getRecTable().getSelectionModel().getSelectedItem();
        if(selected==null) selected = new Recitation(space.getSectionField().getText(), 
                space.getInstructorField().getText(), space.getDayTimeField().getText(), 
                space.getLocationField().getText(),
                data.getTaData().getTA((String)space.getSupervisingCombo().getSelectionModel().getSelectedItem()),
                data.getTaData().getTA((String)space.getSupervisingCombo2().getSelectionModel().getSelectedItem()));
        ClearRecEditTransaction t = new ClearRecEditTransaction(selected, app);
        workspace.getJtps().addTransaction(t);
    }
}
