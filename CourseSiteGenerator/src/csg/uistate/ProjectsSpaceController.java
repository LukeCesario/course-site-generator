/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.uistate;

import csg.CSGApplication;
import csg.Properties.ProjectsProp;
import csg.data.CSGDataComponent;
import csg.data.ProjectStudent;
import csg.data.ProjectTeam;
import csg.transaction.AddStudentTransaction;
import csg.transaction.AddTeamTransaction;
import csg.transaction.ClearStudentEditTransaction;
import csg.transaction.ClearTeamEditTransaction;
import csg.transaction.DeleteStudentTransaction;
import csg.transaction.DeleteTeamTransaction;
import csg.transaction.UpdateStudentTransaction;
import csg.transaction.UpdateTeamTransaction;
import csg.workspace.ProjectsSpace;
import csg.workspace.Workspace;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;

/**
 *
 * @author onear
 */
public class ProjectsSpaceController {

    private CSGApplication app;

    public ProjectsSpaceController(CSGApplication app) {
        this.app = app;
    }

    public boolean handleUpdateTeamMode() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        TableView teamsTable = space.getTeamsTable();
        Object selectedItem = teamsTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return false;
        }
        ProjectTeam team = (ProjectTeam) selectedItem;
        space.getNameField().setText(team.getName());
        space.getColorPicker().setValue(Color.web(team.getColor()));
        space.getTextColorPicker().setValue(Color.web(team.getTextColor()));
        space.getLinkField().setText(team.getLink());
        return true;
    }

    public boolean handleUpdateStudetMode() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        TableView studentsTable = space.getStudentsTable();
        Object selectedItem = studentsTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return false;
        }
        ProjectStudent student = (ProjectStudent) selectedItem;
        space.getFirstNameField().setText(student.getFirstName());
        space.getLastNameField().setText(student.getLastName());
        space.getTeamSelector().getSelectionModel().select(data.getProjectData().getTeam(student.getTeam()));
        space.getRoleField().setText(student.getRole());
        return true;
    }

    public void handleAddTeam() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String name = space.getNameField().getText();
        String color = "#" + space.getColorPicker().getValue().toString().substring(2);
        String textColor = "#" + space.getTextColorPicker().getValue().toString().substring(2);
        String link = space.getLinkField().getText();
        if (name == null || color == null || textColor == null || name.isEmpty() || color.isEmpty() || textColor.isEmpty() || link.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ProjectsProp.EMPTY_TEAM_FIELDS_TITLE), props.getProperty(ProjectsProp.EMPTY_TEAM_FIELDS_MESSAGE));
            return;
        }
        for (ProjectTeam team : data.getProjectData().getTeams()) {
            if (team.getName().equals(name)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ProjectsProp.NAME_USED_TITLE), props.getProperty(ProjectsProp.NAME_USED_MESSAGE));
                return;
            }
        }
        ProjectTeam teamToAdd = new ProjectTeam(name, color, textColor, link);
        AddTeamTransaction t = new AddTeamTransaction(teamToAdd, app);
        workspace.getJtps().addTransaction(t);
        space.getTeamsTable().sort();
        app.getGUI().updateToolbarControls(false);
    }

    public void handleUpdateTeam() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String name = space.getNameField().getText();
        String color = "#" + space.getColorPicker().getValue().toString().substring(2);
        String textColor = "#" + space.getTextColorPicker().getValue().toString().substring(2);
        String link = space.getLinkField().getText();
        if (space.getTeamsTable().getSelectionModel().getSelectedItem() == null) {
            return;
        }
        if (name == null || color == null || textColor == null || name.isEmpty() || color.isEmpty() || textColor.isEmpty() || link.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ProjectsProp.EMPTY_TEAM_FIELDS_TITLE), props.getProperty(ProjectsProp.EMPTY_TEAM_FIELDS_MESSAGE));
            return;
        }
        for (ProjectTeam team : data.getProjectData().getTeams()) {
            if (team.getName().equals(name)&&!team.equals(space.getTeamsTable().getSelectionModel().getSelectedItem())) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ProjectsProp.NAME_USED_TITLE), props.getProperty(ProjectsProp.NAME_USED_MESSAGE));
                return;
            }
        }
        ProjectTeam teamToAdd = new ProjectTeam(name, color, textColor, link);
        UpdateTeamTransaction t = new UpdateTeamTransaction(space.getTeamsTable().getSelectionModel().getSelectedItem(), teamToAdd, app);
        workspace.getJtps().addTransaction(t);
        space.getTeamsTable().sort();
        app.getGUI().updateToolbarControls(false);
    }

    public void handleAddStudent() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String firstName = space.getFirstNameField().getText();
        String lastName = space.getLastNameField().getText();
        String team = (String) space.getTeamSelector().getSelectionModel().getSelectedItem();
        String role = space.getRoleField().getText();
        if (firstName == null || lastName == null || team == null || role == null || firstName.isEmpty()
                || lastName.isEmpty() || role.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ProjectsProp.EMPTY_TEAM_FIELDS_TITLE), props.getProperty(ProjectsProp.EMPTY_STUDENT_FIELDS_MESSAGE));
            return;
        }

        ProjectStudent studentToAdd = new ProjectStudent(firstName, lastName, team, role);
        AddStudentTransaction t = new AddStudentTransaction(studentToAdd, app);
        workspace.getJtps().addTransaction(t);
        space.getTeamsTable().sort();
        app.getGUI().updateToolbarControls(false);
    }

    public void handleUpdateStudent() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String firstName = space.getFirstNameField().getText();
        String lastName = space.getLastNameField().getText();
        String team = (String) space.getTeamSelector().getSelectionModel().getSelectedItem();
        String role = space.getRoleField().getText();
        if (space.getStudentsTable().getSelectionModel().getSelectedItem() == null) {
            return;
        }
        if (firstName == null || lastName == null || team == null || role == null || firstName.isEmpty()
                || lastName.isEmpty() || role.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ProjectsProp.EMPTY_TEAM_FIELDS_TITLE), props.getProperty(ProjectsProp.EMPTY_STUDENT_FIELDS_MESSAGE));
            return;
        }
        ProjectStudent studentToAdd = new ProjectStudent(firstName, lastName, team, role);
        UpdateStudentTransaction t = new UpdateStudentTransaction(space.getStudentsTable().getSelectionModel().getSelectedItem(), studentToAdd, app);
        workspace.getJtps().addTransaction(t);
        space.getTeamsTable().sort();
        app.getGUI().updateToolbarControls(false);
    }

    public void handleDeleteTeam() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectTeam selected = space.getTeamsTable().getSelectionModel().getSelectedItem();
        if (selected == null) {
            return;
        }
        DeleteTeamTransaction t = new DeleteTeamTransaction(selected, app);
        workspace.getJtps().addTransaction(t);
        app.getGUI().updateToolbarControls(false);
    }

    public void handleDeleteStudent() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ProjectStudent selected = space.getStudentsTable().getSelectionModel().getSelectedItem();
        if (selected == null) {
            return;
        }
        DeleteStudentTransaction t = new DeleteStudentTransaction(selected, app);
        workspace.getJtps().addTransaction(t);
        app.getGUI().updateToolbarControls(false);
    }

    public void handleClearEditTeam() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        ProjectTeam selected = (ProjectTeam) space.getTeamsTable().getSelectionModel().getSelectedItem();
        if(selected==null) selected = new ProjectTeam(space.getNameField().getText(), 
                "#" + space.getColorPicker().getValue().toString().substring(2),
                "#" + space.getTextColorPicker().getValue().toString().substring(2),space.getLinkField().getText());
        ClearTeamEditTransaction t = new ClearTeamEditTransaction(selected,app);
        workspace.getJtps().addTransaction(t);
    }

    public void handleClearEditStudent() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ProjectsSpace space = workspace.getProSpace();
        ProjectStudent selected = (ProjectStudent) space.getStudentsTable().getSelectionModel().getSelectedItem();
        if(selected==null) selected = new ProjectStudent(space.getFirstNameField().getText(), space.getLastNameField().getText(),(String)space.getTeamSelector().getSelectionModel().getSelectedItem(), space.getRoleField().getText());
        ClearStudentEditTransaction t = new ClearStudentEditTransaction(selected,app);
        workspace.getJtps().addTransaction(t);
    }

}
