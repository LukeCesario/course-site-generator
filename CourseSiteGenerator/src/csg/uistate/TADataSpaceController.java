/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.uistate;

import csg.CSGApplication;
import csg.data.TeachingAssistant;
import csg.transaction.AddTATransaction;
import csg.workspace.TADataSpace;
import csg.workspace.Workspace;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;

/**
 *
 * @author onear
 */
public class TADataSpaceController {
    
    private CSGApplication app;
    private TADataSpace space;
    private Workspace workspace;
    
    public TADataSpaceController(CSGApplication app){
     this.app = app; 
     
    }
    /**
     * This method responds to when the user requests to add a new TA via the
     * UI. 
     */
    public void handleAddTA() {
        workspace = (Workspace) app.getWorkspaceComponent();
        space = workspace.getTaSpace();
        TextField nameTextField = space.getNameField();
        String name = nameTextField.getText();
        TextField emailTextField = space.getEmailField();
        String email = emailTextField.getText();
        AddTATransaction t = new AddTATransaction(name, email, space,app);
        workspace.getJtps().addTransaction(t);

    }

    public void handleCheckBox(TeachingAssistant ta) {
        
    }

    /**
     * Handles the process of editing an existing TA in the table.
     */
/*
    public void handleUpdateTA() {
        TextField nameTextField = space.getNameField();
        String name = nameTextField.getText();
        TextField emailTextField = space.getEmailField();
        String email = emailTextField.getText();
        TableView taTable = space.getTaTable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        TeachingAssistant ta = (TeachingAssistant) selectedItem;
        //Update_Transaction t = new Update_Transaction(ta.getName(), ta.getEmail(), name, email, app);
       // workspace.getJtps().addTransaction(t);
        nameTextField.requestFocus();
        app.getGUI().updateToolbarControls(false);
    }
*/
    /**
     * Handles the setup of the mode for editing a TA
     *
     * @param k unused
     * @return true if an item in the table is selected and ready to edit,
     * otherwise false
     *//*
    public boolean handleUpdateMode(MouseEvent k) {

       // TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return false;
        }
        TeachingAssistant ta = (TeachingAssistant) selectedItem;

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        workspace.getAddButton().setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));

        workspace.getNameTextField().setText(ta.getName());
        workspace.getEmailTextField().setText(ta.getEmail());

        return true;
    }*/

    /**
     * Deletes A TA and his email from the Table as well as all instances of
     * that TA in the Grid. This is in response to a click + delete action on a
     * TA in the table.
     *
     * @param k KeyEvent triggered by key press
     *//*
    public void handleDeleteTA(KeyEvent k) {
        if (k.getCode().equals(KeyCode.DELETE)) {
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TAData data = (TAData) app.getDataComponent();
            TableView taTable = workspace.getTATable();
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem == null) {
                return;
            }
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            String taName = ta.getName();
            DeleteTA_Transaction t = new DeleteTA_Transaction(data.getTA(taName), app);
            workspace.getJtps().addTransaction(t);
        }
    }*/

    /**
     * Resets the text fields and reverts the add button's function from update
     * to add. As of now this will not count as a transaction.
     *//*
    public void handleClearFields() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        workspace.getNameTextField().clear();
        workspace.getEmailTextField().clear();
        workspace.getNameTextField().requestFocus();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        workspace.getAddButton().setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        workspace.getTATable().getSelectionModel().clearSelection();
    }*/

    /**
     * This function provides a response for when the user clicks on the office
     * hours grid to add or remove a TA to a time slot.
     *
     * @param pane The pane that was toggled.
     *//*
    public void handleCellToggle(Pane pane) {
        if(pane == null) return;
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant) selectedItem;
        if (ta != null) {
            String taName = ta.getName();
            String cellKey = pane.getId();
            Toggle_Transaction t = new Toggle_Transaction(cellKey, taName, pane, app);
            workspace.getJtps().addTransaction(t);
        }
    }*/

    /**
     * Colors the office hour cell that mouse moves over a bright color. All
     * cells in the same day above it and all cells in the same time slot as it
     * are colored a duller color.
     *
     *
     * @param cell The cell which the mouse is over
     * @param grid The TA office hours grid
     *//*
    public void handleMouseEnterCell(Pane cell, GridPane grid) {
        if(cell == null || grid == null) return;
        ObservableList<Node> cells = grid.getChildren();
        int row = GridPane.getRowIndex(cell);
        int column = GridPane.getColumnIndex(cell);
        for (Node node : cells) {
            if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) < column) {
                node.setStyle("-fx-border-color: #b5b11c;");
            }
            if (GridPane.getRowIndex(node) < row && GridPane.getColumnIndex(node) == column) {
                node.setStyle("-fx-border-color: #b5b11c;");
            }
            if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
                node.setStyle("-fx-border-color: #fffa00;");
            }

        }

    }

    /**
     * Resets the coloring effect of handleMouseEnterCell.
     *
     *
     * @param cell that the mouse exits
     * @param grid that the cell is in
     *//*
    public void handleMouseExitCell(Pane cell, GridPane grid) {
        if(cell == null || grid == null) return;
        ObservableList<Node> cells = grid.getChildren();
        for (Node node : cells) {
            if (GridPane.getRowIndex(node) >= 1 && GridPane.getColumnIndex(node) >= 2) {
                node.setStyle("-fx-border-color: #000000;");
            }
            if (GridPane.getRowIndex(node) >= 1 && GridPane.getColumnIndex(node) <= 1) {
                node.setStyle("-fx-border-color: #ffffff;");
            }
            if (GridPane.getRowIndex(node) < 1) {
                node.setStyle("-fx-border-color: #0c2f68 ");
            }

        }

    }
    */
}
