/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.uistate;

import csg.CSGApplication;
import csg.Properties.ScheduleProp;
import csg.data.CSGDataComponent;
import csg.data.ScheduleItem;
import csg.data.ScheduleItemType;
import csg.transaction.AddItemTransaction;
import csg.transaction.ClearItemTransaction;
import csg.transaction.DeleteItemTransaction;
import csg.transaction.UpdateItemTransaction;
import csg.workspace.ScheduleSpace;
import csg.workspace.Workspace;
import djf.ui.AppMessageDialogSingleton;
import java.time.DayOfWeek;
import java.time.LocalDate;
import javafx.scene.control.TableView;
import properties_manager.PropertiesManager;

/**
 *
 * @author onear
 */
public class ScheduleSpaceController {

    private CSGApplication app;

    public ScheduleSpaceController(CSGApplication app) {
        this.app = app;
    }

    public boolean handleUpdateMode() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        TableView schTable = space.getScheduleTable();
        Object selectedItem = schTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return false;
        }
        ScheduleItem item = (ScheduleItem) selectedItem;
        space.getTopicField().setText(item.getTopic());
        space.getCriteriaField().setText(item.getCriteria());
        space.getLinkField().setText(item.getLink());
        space.getTimeField().setText(item.getTime());
        space.getTitleField().setText(item.getTitle());
        space.getRecDatePicker().setValue(item.getDate());
        space.getTypeCombo().getSelectionModel().select(item.getType());
        return true;
    }

    public void handleAddItem() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        ScheduleItemType type = (ScheduleItemType) space.getTypeCombo().getSelectionModel().getSelectedItem();
        LocalDate date = space.getRecDatePicker().getValue();
        String title = space.getTitleField().getText();
        String topic = space.getTopicField().getText();
        String time = space.getTimeField().getText();
        String link = space.getLinkField().getText();
        String criteria = space.getCriteriaField().getText();
        if (date == null || type == null || title == null || topic == null || time == null || link == null || criteria == null
                || title.isEmpty() || topic.isEmpty() || time.isEmpty() || link.isEmpty() || criteria.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ScheduleProp.EMPTY_ITEM_FIELDS_TITLE), props.getProperty(ScheduleProp.EMPTY_ITEM_FIELDS_MESSAGE));
            return;
        }
        ScheduleItem item = new ScheduleItem(type, date, time, title, topic, link, criteria);
        AddItemTransaction t = new AddItemTransaction(item, app);
        workspace.getJtps().addTransaction(t);
        space.getScheduleTable().sort();
        app.getGUI().updateToolbarControls(false);
    }

    public void handleUpdateItem() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        ScheduleItemType type = (ScheduleItemType) space.getTypeCombo().getSelectionModel().getSelectedItem();
        LocalDate date = space.getRecDatePicker().getValue();
        String title = space.getTitleField().getText();
        String topic = space.getTopicField().getText();
        String time = space.getTimeField().getText();
        String link = space.getLinkField().getText();
        String criteria = space.getCriteriaField().getText();
        if (date == null || type == null || title == null || topic == null || time == null || link == null || criteria == null
                || title.isEmpty() || topic.isEmpty() || time.isEmpty() || link.isEmpty() || criteria.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ScheduleProp.EMPTY_ITEM_FIELDS_TITLE), props.getProperty(ScheduleProp.EMPTY_ITEM_FIELDS_MESSAGE));
            return;
        }
        ScheduleItem item = new ScheduleItem(type, date, time, title, topic, link, criteria);
        UpdateItemTransaction t = new UpdateItemTransaction(space.getScheduleTable().getSelectionModel().getSelectedItem(), item, app);
        workspace.getJtps().addTransaction(t);
        space.getScheduleTable().sort();
        app.getGUI().updateToolbarControls(false);
    }

    public void handleDeleteItem() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        ScheduleItem selected = space.getScheduleTable().getSelectionModel().getSelectedItem();
        if (selected == null) {
            return;
        }
        DeleteItemTransaction t = new DeleteItemTransaction(selected, app);
        workspace.getJtps().addTransaction(t);
        app.getGUI().updateToolbarControls(false);
    }

    public void handleClearButton() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        ScheduleItem selected = space.getScheduleTable().getSelectionModel().getSelectedItem();
        if(selected==null) selected = new ScheduleItem((ScheduleItemType)space.getTypeCombo().getSelectionModel().getSelectedItem(),space.getRecDatePicker().getValue(),space.getTimeField().getText(),space.getTitleField().getText(),space.getTopicField().getText(),space.getLinkField().getText(),space.getCriteriaField().getText());
        ClearItemTransaction t = new ClearItemTransaction(selected, app);
        workspace.getJtps().addTransaction(t);
    }

    public void handleStartDate() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        LocalDate newStart = space.getStartDatePicker().getValue();
        if(newStart.isAfter(space.getEndDatePicker().getValue())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Start Date Error", "Start Date Must be Before End Date.");
            return;
        }
        if(!newStart.getDayOfWeek().equals(DayOfWeek.MONDAY)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Monday", "Please Choose a Monday Only");
            return;
        }
        data.getScheduleData().setStartingMondayDay(newStart.getDayOfMonth());
        data.getScheduleData().setStartingMondayMonth(newStart.getMonthValue());
        data.getScheduleData().setStartingMondayYear(newStart.getYear());
    }

    public void handleEndDate() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ScheduleSpace space = workspace.getSchSpace();
        CSGDataComponent data = (CSGDataComponent) app.getDataComponent();
        LocalDate endDate = space.getEndDatePicker().getValue();
        if(endDate.isBefore(space.getStartDatePicker().getValue())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("End Date Error", "End Date Must be After End Date.");
            return;
        }
        if(!endDate.getDayOfWeek().equals(DayOfWeek.FRIDAY)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Friday", "Please Choose a Friday Only");
            return;
        }
        data.getScheduleData().setEndingFridayDay(endDate.getDayOfMonth());
        data.getScheduleData().setEndingFridayMonth(endDate.getMonthValue());
        data.getScheduleData().setEndingFridayYear(endDate.getYear());
    }

}
