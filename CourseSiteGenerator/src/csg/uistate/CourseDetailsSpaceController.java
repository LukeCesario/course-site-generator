package csg.uistate;

import csg.CSGApplication;
import csg.Properties.CourseDetailsProp;
import csg.data.CSGDataComponent;
import csg.data.WebPage;
import csg.workspace.CourseDetailsSpace;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import properties_manager.PropertiesManager;

/**
 *
 * @author Luke Cesario
 */
public class CourseDetailsSpaceController {

    CSGApplication app;
    CSGDataComponent data;
    CourseDetailsSpace space;
    PropertiesManager props;

    private boolean isCtrlDown = false;

    public CourseDetailsSpaceController(CSGApplication app, CourseDetailsSpace space) {
        this.app = app;
        this.space = space;
        data = (CSGDataComponent) app.getDataComponent();
        props = PropertiesManager.getPropertiesManager();
    }

    public void handleSubjectCombo() {
        ComboBox subjectCombo = space.getSubjectCombo();
        String subject = (String) subjectCombo.getSelectionModel().getSelectedItem();
        data.getCourseData().setSubject(subject);
    }

    public void handleNumberCombo() {
        ComboBox numberBox = space.getNumberCombo();
        String numString = (String) numberBox.getSelectionModel().getSelectedItem();
        data.getCourseData().setNumber(Integer.parseInt(numString));
    }

    public void handleSemesterCombo() {
        ComboBox semesterBox = space.getSemesterCombo();
        String semester = (String) semesterBox.getSelectionModel().getSelectedItem();
        data.getCourseData().setSemester(semester);

    }

    public void handleYearCombo() {
        ComboBox yearBox = space.getYearCombo();
        String year = (String) yearBox.getSelectionModel().getSelectedItem();
        data.getCourseData().setYear(Integer.parseInt(year));
    }

    public void handleTitleField() {
        data.getCourseData().setTitle(space.getTitleField().textProperty().get());
    }

    public void handleInstructorNameField() {
        data.getCourseData().setInstructorName(space.getInstructorNameField().textProperty().get());
    }

    public void handleInstructorHomeField() {
        data.getCourseData().setInstructorHome(space.getInstructorHomeField().textProperty().get());
    }

    public boolean handleChangeExportDirButton(ActionEvent e) {
        DirectoryChooser choose = new DirectoryChooser();
        File dir = choose.showDialog(app.getGUI().getWindow());
        if (dir != null) {
            data.getCourseData().setExportDir(dir.getAbsolutePath());
            space.getExportDirLabel().setText(data.getCourseData().getExportDir());
            return true;
        }
        return false;
    }

    public boolean handleSelectTemplateDirButton() {
        DirectoryChooser choose = new DirectoryChooser();
        File dir = choose.showDialog(app.getGUI().getWindow());
        if (dir != null) {
            File indexFile = new File(dir.getAbsolutePath() + "/index.html");
            File syllabusFile = new File(dir.getAbsolutePath() + "/syllabus.html");
            File scheduleFile = new File(dir.getAbsolutePath() + "/schedule.html");
            File hwsFile = new File(dir.getAbsolutePath() + "/hws.html");
            File projectsFile = new File(dir.getAbsolutePath() + "/projects.html");
            if (indexFile.exists()) {
                WebPage indexInfo = new WebPage(space.getHomeCheckBox(), "Home", "index.html", "HomeBuilder.js");
                space.getSitePagesTable().getItems().add(indexInfo);
            }
            if (syllabusFile.exists()) {
                WebPage syllabusInfo = new WebPage(space.getSyllabusCheckBox(), "Syllabus", "syllabus.html", "SyllabusBuilder.js");
                space.getSitePagesTable().getItems().add(syllabusInfo);
            }
            if (scheduleFile.exists()) {
                WebPage scheduleInfo = new WebPage(space.getScheduleCheckBox(), "Schedule", "schedule.html", "WebPageBuilder.js");
                space.getSitePagesTable().getItems().add(scheduleInfo);
            }
            if (hwsFile.exists()) {
                WebPage hwsInfo = new WebPage(space.getHwsCheckBox(), "HWs", "hws.html", "HWsBuilder.js");
                space.getSitePagesTable().getItems().add(hwsInfo);
            }

            if (projectsFile.exists()) {
                WebPage projectsInfo = new WebPage(space.getProjectsCheckBox(), "Projects", "projects.html", "ProjectsBuilder.js");
                space.getSitePagesTable().getItems().add(projectsInfo);
            }
            space.getTemplateDirLabel().setText(dir.getAbsolutePath());
            data.setTemplateDir(dir.getAbsolutePath());
            return true;
        }
        return false;
    }

    public void handleChangeImageButton(ImageView view) {
        FileChooser choose = new FileChooser();
        choose.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        choose.setTitle(props.getProperty(CourseDetailsProp.IMAGE_SELECTOR_TITLE));
        File imageFile = choose.showOpenDialog(app.getGUI().getWindow());
        if (imageFile != null) {
            try {
                view.setImage(new Image(imageFile.toURI().toURL().toString()));
            } catch (MalformedURLException ex) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(CourseDetailsProp.IMAGE_LOADING_ERROR_TITLE), props.getProperty(CourseDetailsProp.TROUBLE_LOADING_IMAGE_MESSAGE));
            }
            System.out.println("Set");
        }
    }

}
