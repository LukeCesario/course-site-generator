/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.file;

import csg.CSGApplication;
import csg.data.CSGDataComponent;
import csg.data.ProjectStudent;
import csg.data.ProjectTeam;
import csg.data.Recitation;
import csg.data.ScheduleItem;
import csg.data.ScheduleItemType;
import csg.data.TeachingAssistant;
import static csg.file.CSGFileComponent.loadJSONFile;
import djf.components.AppDataComponent;
import java.io.File;
import java.time.LocalDate;
import java.util.Date;
import javafx.collections.FXCollections;
import javax.json.JsonObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author onear
 */
public class CSGFileComponentTest {
    
    public CSGFileComponentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

 

    /**
     * Test of loadData method, of class CSGFileComponent.
     */
    @Test
    public void testLoadData() throws Exception {
        System.out.println("loadData");
        CSGApplication app = new CSGApplication();
        app.loadProperties("app_properties.xml");
        AppDataComponent data = new CSGDataComponent(app);
        //MAKE PATH RELATIVE?
        File file = new File("../CourseSiteGenerator/src/csg/test_bed/SiteSaveTest.json");
        String filePath = (file.getAbsolutePath());
        CSGFileComponent instance = new CSGFileComponent(app);
        instance.loadData(data, filePath);
        CSGDataComponent csgData = (CSGDataComponent) data;
        //TEST COURSE DETAILS LOAD
        assertEquals("Subject", "CSE", csgData.getCourseData().getSubject());
        assertEquals("Semester", "Spring", csgData.getCourseData().getSemester());
        assertEquals("Number",219,csgData.getCourseData().getNumber());
        assertEquals("Year", 2017, csgData.getCourseData().getYear());
        assertEquals("Title","Computer Science III",csgData.getCourseData().getTitle());
        assertEquals("Instructor", "Ritwik Banerjee", csgData.getCourseData().getInstructorName());
        assertEquals("Instructor Home", "http://www3.cs.stonybrook.edu/~rbanerjee/teaching/cse219/", csgData.getCourseData().getInstructorHome() );
        //TEST OH LOAD: incomplete?
        assertEquals("OH Start Hour", "1", csgData.getTaData().getStartHour() + "");
        assertEquals("OH End Hour", "24", csgData.getTaData().getEndHour() + "");
        //TEST TA LOAD
        assertEquals("TA1", new TeachingAssistant("Jane Doe", "jane.doe@sbu.edu", true), csgData.getTaData().getTeachingAssistants().get(0));
        assertEquals("TA2", new TeachingAssistant("Joe Shmoe", "joe.shmoe@sbu.edu", true), csgData.getTaData().getTeachingAssistants().get(1));
        assertEquals("TA3", new TeachingAssistant("John Shmo", "john.shmo@sbu.edu", false), csgData.getTaData().getTeachingAssistants().get(2));
        assertEquals("TA4", new TeachingAssistant("Phil Gray", "phil.gray@sbu.edu", false), csgData.getTaData().getTeachingAssistants().get(3));
        //TEST RECITATION LOAD
        assertEquals("Rec1", new Recitation("Banerjee", "R02", "Wed 3:30pm-4:23pm","Old CS 2114", 
        new TeachingAssistant("Jane Doe", "jane.doe@sbu.edu", true), new TeachingAssistant("John Shmo", "john.shmo@sbu.edu", false)),
                csgData.getRecData().getRecitations().get(0));
        //TEST SCHEDULE LOAD
        assertEquals("Item1", new ScheduleItem(ScheduleItemType.HOLIDAY,LocalDate.of(2017,1,23), "1:00pm", "SNOW DAY", "NA", "NA", "NA"),
                csgData.getScheduleData().getHolidayItems().get(0));
        assertEquals("Item2", new ScheduleItem(ScheduleItemType.HOLIDAY,LocalDate.of(2017,4,23), "1:00pm", "FREEDOM", "NA", "NA", "NA"),
                csgData.getScheduleData().getHolidayItems().get(1));
        //TEST STUDENTS LOAD
        assertEquals("Student1", new ProjectStudent("Neal", "Beeken", "Atomic Comics", "leader"), csgData.getProjectData().getStudents().get(0));
        //TEST TEAMS LOAD
        ProjectTeam testTeam = new ProjectTeam("Atomic Comics", "000000", "ffffff", "http://atomicomic.com");
        testTeam.addMember(new ProjectStudent("Neal", "Beeken", "Atomic Comics", "leader"));
        assertEquals("Team1", testTeam, csgData.getProjectData().getTeams().get(0));
    }   

 

    /**
     * Test of loadJSONFile method, of class CSGFileComponent.
     */
    /*@Test
    public void testLoadJSONFile() throws Exception {
        System.out.println("loadJSONFile");
        String jsonFilePath = "";
        JsonObject expResult = null;
        JsonObject result = CSGFileComponent.loadJSONFile(jsonFilePath);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
}
